import './App.scss';
import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';

import Router from '../Router/Router';

import { store } from '../../store/configureStore';

class App extends PureComponent {

  render() {
    return (
      <Provider store={ store }>
        <Router />
      </Provider>
    )
  }
}

export default App;
