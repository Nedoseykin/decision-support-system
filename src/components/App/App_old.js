import React, { Component } from 'react';
import './App.scss';
// import { database } from '../../database/firebase';
import { getData, orderByAge, writeData } from '../../database/utils_firebase';
import { isNotEmptyArray } from '../../utils/utils';
// import myFirebase from './firebase';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: 0,
      patients: [],
    };
  }

  componentDidMount() {
    // database.ref('/').on('value', () => {
    //   console.log('The DATA have changed!');
    // });
    getData(patients => {
      this.setState({ patients });
    });
  }

  componentDidUpdate( prevProps, prevState, snapshot ) {
    if ( prevState.patients !== this.state.patients ) {
      writeData( this.state.patients );
    }
  }

  handleChange = name => EO => {
    EO.preventDefault();
    this.setState({ [ name ]: EO.target.value });
  };

  handleDeleteAll = () => {
    this.setState({ patients: [] }, this.resetForm);
  };

  handleSubmit = () => {
    let { name, age, patients } = this.state;
    if ( !!name && !!age ) {
      patients = Array.isArray(patients)
        ? patients.slice()
        : [];
      patients.push({ name, age });
      this.setState({ patients }, this.resetForm);
    }
  };

  handleOrderByAge = () => {
    orderByAge();
  };

  resetForm = () => {
    this.setState({ name: '', age: 0 });
  };

  getList = () => {
    const { patients } = this.state;
    return isNotEmptyArray(patients)
      ? patients.map((p, i) => {
        const { name, age } = p;
        return (
          <div
            key={ i }
            className='list-item'
          >
            { `Name: ${ name }, age: ${ age }` }
          </div>
        )
      })
      : 'No data to display';
  };

  getForm = () => {
    const { name, age } = this.state;
    return ([
      <div key='name' className='form-item row-flex-wrap-start-start'>
        <div
          key='label'
          className='col-flex-16-16 col-flex-lg-8-16'
        >
          <label htmlFor="name">Name</label>
        </div>
        <div
          key='input'
          className='col-flex-16-16 col-flex-lg-8-16'
        >
          <input
            type="text"
            id='name'
            value={ name || '' }
            onChange={ this.handleChange('name') }
          />
        </div>
      </div>,
      <div key='age' className='form-item row-flex-wrap-start-start'>
        <div key='label' className='col-flex-16-16 col-flex-lg-8-16'>
          <label htmlFor="age">Age</label>
        </div>
        <div key='input' className='col-flex-16-16 col-flex-lg-8-16'>
          <input
            type="text"
            id='age'
            value={ age || 0 }
            onChange={ this.handleChange('age') }
          />
        </div>
      </div>,
      <div key='toolbar' className='toolbar row-flex-wrap-start-start'>
        <div key='submit'
             className='toolbar-button-box col-flex-16-16 col-flex-sm-8-16 col-flex-lg-4-16'
        >
          <button
            type="submit"
            id='submit'
            onClick={this.handleSubmit}
          >
            Add patient
          </button>
        </div>
        <div
          key='deleteAll'
          className='toolbar-button-box col-flex-16-16 col-flex-sm-8-16 col-flex-lg-4-16'
        >
          <button
            type="button"
            id='deleteAll'
            onClick={this.handleDeleteAll}
          >
            Clear database
          </button>
        </div>
        <div
          key='orderByAge'
          className='toolbar-button-box col-flex-16-16 col-flex-sm-8-16 col-flex-lg-4-16'
        >
          <button
            type="button"
            id='orderByAge'
            onClick={this.handleOrderByAge}
          >
            Order by age
          </button>
        </div>
      </div>
    ])
  };

  render() {
    return (
      <div className="App">
        <div key='header' className='App-header container-flex-column-nowrap-center-start'>
          <div className='row-flex-nowrap-start-center'>
            <div className = 'header-title col-flex-10-16'>
              <h1>
                Test project to learn Firebase
              </h1>
            </div>
          </div>
        </div>
        <div key='content' className='App-content'>
          <div key='contentTitle' className='content-title'>
            <h2>
              There will be some info from firebase
            </h2>
          </div>
          <div key='contentMain' className='content-main container-flex-column-nowrap-start-start'>
            <div
              key='card'
              className='card row-flex-wrap-start-start'
            >
              <div
                key='list'
                className='card-list col-flex-16-16 col-flex-lg-6-16'
              >
                { this.getList() }
              </div>
              <div
                key='form'
                className='card-form col-flex-16-16 col-flex-lg-10-16'
              >
                { this.getForm() }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
