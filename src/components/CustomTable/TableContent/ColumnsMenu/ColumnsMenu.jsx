import './ColumnsMenu.scss';
import React, { useEffect } from 'react';
import ReactDom from 'react-dom';
import classNames from 'classnames';

import Scrollbars from '../../../CustomScrollbars/CustomScrollbars';
import Checkbox from '../../../controls/Checkbox/Checkbox';
import MenuHead from './MenuHead';
import { isNotEmpty } from '../../../../utils/utils';

const ColumnsMenu = props => {
  const { x, y, open, columns, cbClosed, cbChanged } = props;
  let menu = null;

  const handleDocumentMouseUp = event => {
    event.preventDefault();
    event.stopPropagation();
    if (open) {
      const { clientX, clientY } = event;
      const m = ReactDom.findDOMNode(menu);
      if (!!m) {
        const { left, top, width, height } = m.getBoundingClientRect();
        let menuClick = (clientX >= left)
          && (clientX <= (left + width))
          && (clientY >= top)
          && (clientY <= (top + height));
        !menuClick && !!cbClosed && cbClosed(event);
      }
    }
  };

  const handleItemCheck = (id, name) => () => {
    console.log('handleItemCheck: id: ', id, ', name: ', name);
    const newColumns = columns.map(col => ({
      ...col,
      [ name ]: col.id === id ? !col[ name ]: col[ name ],
    }));
    !!cbChanged && newColumns.filter(c => c.visible).length > 0 && cbChanged(newColumns);
  };

    useEffect(() => {
    if (open === true) {
      document.addEventListener('mouseup', handleDocumentMouseUp, false);
    } else {
      document.removeEventListener('mouseup', handleDocumentMouseUp);
    }
  });

  const cols = columns.map(c => {
    const { id, columnName, visible, fixed } = c;
    const label = isNotEmpty(columnName) ? columnName : id;
    return (
      <div
        key={ id }
        className={ classNames('columns-menu--item') }
      >
        <div
          key='leftBox'
          className='item--left-box'
        >
          <div
            key='checkbox'
            className='item--checkbox-box'
            onClick={ !!cbClosed ? cbClosed : null }
          >
            <Checkbox
              withLabel={ false }
              value={ visible }
              iconSize={ 14 }
              onChange={ handleItemCheck(id, 'visible') }
            />
          </div>
          <div key='label' className='item--label-box'>
            { label }
          </div>
        </div>
        <div
          key='rightBox'
          className='item--right-box'
        >
          <div
            key='checkboxFixed'
            className='item--checkbox-box'
          >
            <Checkbox
              withLabel={ false }
              value={ fixed }
              iconSize={ 14 }
              onChange={ handleItemCheck(id, 'fixed') }
            />
          </div>
        </div>

      </div>
    )
  });
  return (
    <div
      className={ classNames('ColumnsMenu') }
      data-visible={ `${ open }` }
      style={{ left: x, top: y }}
      ref={ elm => { menu = elm; } }
    >
      <div className='columns-menu--content'>
        <MenuHead columns={ columns } onChange={ !!cbChanged ? cbChanged : null } />
        <div key='items' className='columns-menu--content--items'>
          <Scrollbars>
            { cols }
          </Scrollbars>
        </div>
      </div>
    </div>
  )
};

export default ColumnsMenu;
