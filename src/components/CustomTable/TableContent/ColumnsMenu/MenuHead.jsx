import React from 'react';
import { isNotEmptyArray } from '../../../../utils/utils';

import Checkbox from '../../../controls/Checkbox/Checkbox';

const MenuHead = props => {
  const { columns, onChange } = props;

  const handleChange = name => () => {
    // console.log(columns.slice());
    const status = isNotEmptyArray( columns )
      ? columns.filter(c => c[ name ]).length === columns.length
      : false;
    // console.log('status: ', status);
    const newColumns = isNotEmptyArray(columns)
      ? columns.map(c => ({ ...c, fixed: !status }))
      : columns;
    !!onChange && onChange(newColumns);
  };

  const fixedAll = () => isNotEmptyArray(columns)
    ? columns.length === columns.filter(c => c.fixed).length
    : false;

  return (
    <div className='columns-menu--content--head'>
      <div
        key='label'
        className='head--label'
      >
        Column name
      </div>
      <div
        key='fixedAll'
        className='head--fixed-all'
      >
        <div
          key='checkbox'
          className='fixed-all--checkbox'
        >
          <Checkbox
            withLabel={ false }
            value={ fixedAll() }
            iconSize={ 14 }
            onChange={ handleChange('fixed') }
          />
        </div>
        <div
          key='label'
          className='fixed-all--label'
        >
          Fixed
        </div>
      </div>
    </div>
  )
};

export default MenuHead;
