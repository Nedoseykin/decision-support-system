import React, { PureComponent, Fragment } from 'react';

import Scrollbars from '../../CustomScrollbars/CustomScrollbars';
import Th from './Th/Th';
import ShadowTh from './ShadowTh/ShadowTh';
import ColumnsMenu from './ColumnsMenu/ColumnsMenu';

import { findArrayItem, findArrayItemIndex, isNotEmptyArray } from '../../../utils/utils';

class TableContent extends PureComponent {

  /*const {
    rows,
    htmlId,
    parentId,
    cbColumnsChanged,
    cbRowDoubleClicked,
    selfScrollTop,
    cbScrolled,
  } = props;*/

  // let { columns } = props;

  // const [ contextMenuData, setContextMenuData ] = useState({ open: 'initial', x: 0, y: 0 });

  constructor(props) {
    super(props);
    this.state = {
      contextMenuData: { open: 'initial', x: 0, y: 0 },
    };
    this.scrollableHead     = null;
    this.fixedContent       = null;
    this.scrollableScroller = null;
    // this.fixedScroller      = null;
  }

  // let scrollableHead     = null;
  // let fixedContent       = null;
  // let scrollableScroller = null;
  // let fixedScroller      = null;

  calculateTableData = columns => {
    const fixed = columns.filter(c => ( c.fixed && c.visible ));
    let fixedWidth = 0;
    fixed.forEach(f => { fixedWidth += f.width });
    const scrollable = columns.filter(c => (!c.fixed && c.visible));
    let scrollableFullWidth = 0;
    scrollable.forEach(s => { scrollableFullWidth += s.width });
    return { fixed, fixedWidth, scrollable, scrollableFullWidth };
  };

  cbColumnWidthChanged = (columnId, columnWidth) => {
    let { columns, cbColumnsChanged } = this.props;
    let newColumns = columns;
    if (isNotEmptyArray(columns)) {
      newColumns = columns.map(c => ({
        ...c,
        width: c.id === columnId ? columnWidth : c.width,
      }))
    }
    !!cbColumnsChanged && cbColumnsChanged(newColumns);
  };

  handleRowDoubleClick = rowId => () => {
    const { cbRowDoubleClicked } = this.props;
    !!cbRowDoubleClicked && cbRowDoubleClicked( rowId );
  };

  handleColumnOrderChange = ordered => {
    let { columns, cbColumnsChanged } = this.props;
    let newColumns = [];
    columns = columns.slice();
    ordered.forEach(item => {
      let { id, fixed } = item;
      fixed = fixed === 'true';
      const i = findArrayItemIndex(columns, { id });
      if (i > -1) {
        const c = columns.splice(i, 1)[ 0 ];
        newColumns.push({ ...c, fixed });
      }
    });
    newColumns = newColumns.concat(columns);
    !!cbColumnsChanged && cbColumnsChanged(newColumns);
  };

  handleToggleContextMenu = open => event => {
    event.preventDefault();
    event.stopPropagation();
    const { htmlId } = this.props;
    const { contextMenuData } = this.state;
    let { x, y } = contextMenuData;
    if (open === true) {
      const { clientX, clientY } = event;
      const table = document.getElementById(htmlId);
      if (!table)return;
      const menu = table.querySelector('.ColumnsMenu');
      if (!menu) return;
      const { left: tLeft, top: tTop } = table.getBoundingClientRect();
      x = clientX - tLeft;
      y = clientY - tTop;
    }
    this.setState({ contextMenuData: { open, x, y }, });
  };

  cbScrollableChangeScroll = (scrollLeft, scrollTop, scrollWidth, scrollHeight, offsetWidth, offsetHeight) => {
    const { cbScrolled } = this.props;
    //setSelfScrollTop(scrollTop);
    !!cbScrolled && cbScrolled({
      scrollLeft,
      scrollTop,
      scrollWidth,
      scrollHeight,
      offsetWidth,
      offsetHeight
    });
    if (!!this.fixedContent) {
      const elm = this.fixedContent.querySelector('.custom-scrollbars--content');
      if ( !!elm ) {
        elm.scrollTop = scrollTop;
      }
    }
    if (!!this.scrollableHead) {
      this.scrollableHead.scrollLeft = scrollLeft;
    }
  };

  cbFixedChangeScroll = (scrollLeft, scrollTop, scrollWidth, scrollHeight, offsetWidth, offsetHeight) => {
    const { cbScrolled, } = this.props;
    //setSelfScrollTop(scrollTop);
    !!cbScrolled && cbScrolled({
      scrollLeft,
      scrollTop,
      scrollWidth,
      scrollHeight,
      offsetWidth,
      offsetHeight,
    });
    if ( !!this.scrollableScroller ) this.scrollableScroller(scrollLeft, scrollTop);
  };

  FixedTable = props => {
    const {
      columns,
      rows,
      width,
      htmlId,
      // parentId,
      cbColumnWidthChanged,
      onRowDoubleClick,
      cbColumnOrderChanged,
      cbColumnContextMenuClicked,
    } = props;

    return isNotEmptyArray( columns )
      ? (
        <div className='custom-table--fixed' style={{ width }}>
          <div
            key='fixedHeader'
            className='fixed--head'
          >
            <div
              key='head-row'
              className='custom-table--tr'
            >
              {
                columns.map(th => (
                  <Th
                    key={ th.id }
                    htmlId={ htmlId }
                    { ...th }
                    cbColumnWidthChanged={ cbColumnWidthChanged }
                    cbColumnOrderChanged={ cbColumnOrderChanged }
                    cbColumnContextMenuClicked={ cbColumnContextMenuClicked }
                  />
                ))
              }
            </div>
          </div>
          <div
            key='fixedBody'
            className='fixed--body'
          >
            <Scrollbars
              isHorizontal={ false }
              isVertical={ false }
              disableHorizontalScroll={ true }
              getRef={ elm => { this.fixedContent = elm } }
              getScroller={ scroller => { this.fixedScroller = scroller; } }
              onChange={ this.cbFixedChangeScroll }
            >
              {
                isNotEmptyArray(rows) && (
                  rows.map(tr => {
                    const { id: trId, cells } = tr;
                    return (
                      <div
                        key={ `tr-${ trId }` }
                        className='custom-table--tr'
                        onDoubleClick={ onRowDoubleClick( trId ) }
                      >
                        {
                          columns.map(td => {
                            const { id, width } = td;
                            const cell = findArrayItem( cells, { id } );
                            const columnHtmlId = `${ htmlId }-${ id }`;
                            const value = !!cell ? cell.value : null;
                            return (
                              <div
                                key={ `td-${ id }` }
                                className={ `custom-table--td ${ id }` }
                                data-column_id={ columnHtmlId }
                                style={{ width: width }}
                              >
                                { value }
                              </div>
                            )
                          })
                        }
                      </div>
                    )
                  })
                )
              }
            </Scrollbars>
          </div>
        </div>
      )
      : null;
  };

  ScrollableTable = props => {
    const {
      columns,
      rows,
      fullWidth,
      fixedWidth,
      htmlId,
      // parentId,
      cbColumnWidthChanged,
      onRowDoubleClick,
      cbColumnOrderChanged,
      cbColumnContextMenuClicked,
    } = props;
    return (
      <div className='custom-table--scrollable' style={{ width: `calc(100% - ${ fixedWidth }px)` }}>
        <div
          key='scrollable-head'
          className='scrollable--head'
          style={{ width: '100%' }}
          ref={ elm => { this.scrollableHead = elm } }
        >
          <div
            key={ `head-row` }
            className='custom-table--tr'
            style={{ width: fullWidth }}
          >
            {
              columns.map(th => (
                <Th
                  key={ th.id }
                  htmlId={ htmlId }
                  { ...th }
                  cbColumnWidthChanged={ cbColumnWidthChanged }
                  cbColumnOrderChanged={ cbColumnOrderChanged }
                  cbColumnContextMenuClicked={ cbColumnContextMenuClicked }
                />
              ))
            }
          </div>
        </div>
        <div
          key='scrollable-body'
          className='scrollable--body'
          style={{ width: '100%' }}
        >
          <Scrollbars
            hideScrollbars={ true }
            getRef={ elm => { /*scrollableContent = elm*/ } }
            getScroller={ scroller => { this.scrollableScroller = scroller; } }
            onChange={ this.cbScrollableChangeScroll }
          >
            {
              isNotEmptyArray(rows) &&
              rows.map(tr => {
                const { id: trId, cells } = tr;
                return (
                  <div
                    key={ `body-row-${ trId }` }
                    className='custom-table--tr'
                    style={{ width: fullWidth }}
                    onDoubleClick={ onRowDoubleClick( trId ) }
                  >
                    {
                      columns.map(td => {
                        const { id, width } = td;
                        const cell = findArrayItem( cells, { id } );
                        const columnHtmlId = `${ htmlId }-${ id }`;
                        const value = !!cell ? cell.value : null;
                        return (
                          <div
                            key={ id }
                            className={ `custom-table--td ${ id }` }
                            data-column_id={ columnHtmlId }
                            style={{ width }}
                          >
                            { value }
                          </div>
                        )
                      })
                    }
                  </div>
                )
              })
            }
          </Scrollbars>
        </div>
      </div>
    );
  };

  render() {
    const {
      rows,
      htmlId,
      parentId,
      cbColumnsChanged,
      // cbRowDoubleClicked,
      selfScrollTop,
      // cbScrolled,
    } = this.props;

    let { columns } = this.props;

    const { contextMenuData } = this.state;

    const {
      fixed,
      scrollable,
      fixedWidth,
      scrollableFullWidth,
    } = this.calculateTableData(columns);

    const FixedTable = this.FixedTable;
    const ScrollableTable = this.ScrollableTable;
    return (
      <Fragment>
        <FixedTable
          parentId={ parentId }
          htmlId={ htmlId }
          columns={ fixed }
          rows={ rows }
          width={ fixedWidth }
          scrollTop={ selfScrollTop }
          cbColumnWidthChanged={ this.cbColumnWidthChanged }
          onRowDoubleClick={ this.handleRowDoubleClick }
          cbColumnOrderChanged={ this.handleColumnOrderChange }
          cbColumnContextMenuClicked={ this.handleToggleContextMenu(true) }
        />
        <ScrollableTable
          parentId={ parentId }
          htmlId={ htmlId }
          columns={ scrollable }
          rows={ rows }
          fullWidth={ scrollableFullWidth }
          fixedWidth={ fixedWidth }
          scrollTop={ selfScrollTop }
          cbColumnWidthChanged={ this.cbColumnWidthChanged }
          onRowDoubleClick={ this.handleRowDoubleClick }
          cbColumnOrderChanged={ this.handleColumnOrderChange }
          cbColumnContextMenuClicked={ this.handleToggleContextMenu(true) }
        />
        <ShadowTh />
        <ColumnsMenu
          open={ contextMenuData.open }
          x={ contextMenuData.x }
          y={ contextMenuData.y }
          columns={ columns }
          cbChanged={ cbColumnsChanged }
          cbClosed={ this.handleToggleContextMenu(false) }
        />
      </Fragment>
    )
  };
}

export default TableContent;
