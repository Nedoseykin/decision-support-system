import './DynamicTable.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Table from '../CustomTable/CustomTable';

class DynamicTable extends PureComponent {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    rows: PropTypes.arrayOf(PropTypes.object).isRequired,
    cbColumnsChanged: PropTypes.func,
  };

  static defaultProps = {
    cbColumnsChanged: null,
  };

  render() {
    const {
      columns,
      rows,
      cbColumnsChanged,
    } = this.props;

    return (
      <Table
        columns={ columns }
        rows={ rows }
        cbColumnsChanged={ cbColumnsChanged }
      />
    )
  }
}

export default DynamicTable;
