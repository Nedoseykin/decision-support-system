import './PageBuilder.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isNotEmpty } from '../../utils/utils';

class PageBuilder extends PureComponent {
  static propTypes = {
    className   : PropTypes.string,
    header      : PropTypes.func,
    mainSection : PropTypes.func,
    toolbar     : PropTypes.func,
  };

  static defaultProps = {
    className   : null,
    header      : null,
    mainSection : null,
    toolbar     : null,
  };

  classCSS = 'PageBuilder';

  getHeader = () => {
    const { header } = this.props;
    return !!header ? header() : null;
  };

  getMainSection = () => {
    const { mainSection } = this.props;
    return !!mainSection ? mainSection() : null;
  };

  getToolBar = () => {
    const { toolbar } = this.props;
    return !!toolbar ? toolbar() : null;
  };

  render() {
    // console.log('PageBuilder props: ', this.props);
    const {
      className,
    } = this.props;
    const header = this.getHeader();
    const mainSection = this.getMainSection();
    const toolBar = this.getToolBar();
    return (
      <div
        className={ classNames( this.classCSS, isNotEmpty( className ) && className ) }
      >
        { !!header && header }
        { !!mainSection && mainSection }
        { !!toolBar && toolBar }
      </div>
    )
  }
}

export default PageBuilder;
