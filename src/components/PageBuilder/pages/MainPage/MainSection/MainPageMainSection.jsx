import './MainPageMainSection.scss';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import TextInput from '../../../../controls/TextInput/TextInput';
import DateInput from '../../../../controls/DateInput/DateInput';
import RadioInput from '../../../../controls/RadioInput/RadioInput';
import CustomScrollbars from '../../../../CustomScrollbars/CustomScrollbars';
import CustomTable from '../../../../CustomTable/CustomTable';
import Checkbox from '../../../../controls/Checkbox/Checkbox';
import { acSetValue, acUiUpdateTableData } from '../../../../../store/actions';
import { dateToDDMMYYY } from '../../../../../utils/utils';
import { SEX } from '../../../../../consts/common_consts';

const mapStateToProps = state => ({
  firstName    : state.patient.firstName,
  thirdName    : state.patient.thirdName,
  familyName   : state.patient.familyName,
  birthDate    : state.patient.birthDate,
  sex          : state.patient.sex,
  hospitalized : state.patient.hospitalized,

  table: state.ui.table,
});

@connect(mapStateToProps)
class MainPageMainSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleChangeValue = name => value => {
    // console.log('name: ', name, ', value: ', value);
    const { dispatch, } = this.props;
    dispatch(acSetValue({ name, value }));
  };

  cbCustomTable_columnsChanged = columns => {
    this.props.dispatch(acUiUpdateTableData({ columns }));
  };

  cbCustomTable_rowDoubleClicked = rowId => {
    console.log('Row: ', rowId);
  };

  render() {
    const {
      firstName,
      thirdName,
      familyName,
      birthDate,
      sex,
      table,
      hospitalized,
    } = this.props;
    const sexValuesList = [
      { id: SEX.MAIL, label: 'Мужчина' },
      { id: SEX.FEMAIL, label: 'Женщина' }
    ];

    /*
    *

    * */

    return (
      <div className='page--main-section'>
        <h2 className='main-section--title'>Main page main section</h2>
        <hr/>
        <div className='main-section--central-panel'>
          <CustomScrollbars>
            <div key='form' className='main-section--form container-flex-column-nowrap-start-start'>
              <div key='name' className='row-flex-wrap-start-start'>
                <div
                  key='familyName'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <TextInput
                    label='Фамилия'
                    value={ familyName || '' }
                    onChange={ this.handleChangeValue('familyName') }
                  />
                </div>
                <div
                  key='firstName'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <TextInput
                    label='Имя'
                    value={ firstName || '' }
                    onChange={ this.handleChangeValue('firstName') }
                  />
                </div>
                <div
                  key='thirdName'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-6-16'
                >
                  <TextInput
                    label='Отчество'
                    value={ thirdName || '' }
                    onChange={ this.handleChangeValue('thirdName') }
                  />
                </div>
              </div>
              <div key='row2' className='row-flex-wrap-start-start'>
                <div
                  key='birthDate'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <DateInput
                    label='Дата рождения'
                    type='text'
                    value={ birthDate }
                    valueConverter={ value => dateToDDMMYYY(value) }
                    onChange={ this.handleChangeValue('birthDate') }
                  />
                </div>
                <div
                  key='sex'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <RadioInput
                    label='Пол'
                    value={ sex }
                    itemsList={ sexValuesList }
                    onChange={ this.handleChangeValue('sex') }
                  />
                </div>
              </div>
              <div key='row3' className='row-flex-wrap-start-start'>
                <div
                  key='testScrollbars'
                  style={{ height: 100 }}
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <div className='testScrollbars' style={{ width: 300, height: 100, border: '1px solid rgba(0,0,0,0.5)' }}>
                    <CustomScrollbars>
                      <div className='testContainer' style={{ width: 450, backgroundColor: 'rgba(0,0,0,0.05)' }}>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque debitis dignissimos
                        eaque harum in nesciunt sapiente. A accusamus ad aliquid amet aperiam blanditiis
                        exercitationem explicabo facilis, illum laborum molestiae nam pariatur, porro quia quo,
                        recusandae reprehenderit repudiandae sed temporibus veniam. Accusamus amet, atque
                        doloribus dolorum eligendi eveniet fuga iusto labore libero nesciunt, quas quod
                        repellendus rerum vero voluptate! Ad dolore, dolorem doloremque, ea eaque excepturi
                        facilis iure laboriosam laborum laudantium maxime modi nisi quam quisquam reprehenderit
                        rerum tempora totam voluptas! Aliquam architecto asperiores aut, cum, cumque, esse est
                        facilis fuga illum iusto minima nisi nostrum pariatur porro possimus quasi quidem velit?
                        Commodi delectus expedita quaerat. Alias aliquid animi asperiores assumenda aut
                        consequatur cumque dicta dolorem ea eligendi enim eos ex expedita iusto laboriosam
                        laudantium minus, modi nihil nobis obcaecati odio odit possimus provident quae quas
                        quasi qui, quia, quod rem repellat repellendus reprehenderit sint sit suscipit ut
                        veniam voluptate. A cupiditate, doloremque doloribus ducimus enim esse itaque iure
                        minima, nisi odit praesentium quae rerum sapiente unde veritatis! Enim, maxime, vel.
                        Accusantium alias architecto, autem culpa dolore et exercitationem expedita explicabo
                        fuga harum, id illo incidunt laborum laudantium minima mollitia odio officiis provident
                        quae quis repellat reprehenderit sapiente sed suscipit unde velit veritatis, voluptatem!
                        Ad culpa error porro. A accusamus accusantium aliquid, consequatur delectus dolorem
                        dolores eaque error eum exercitationem explicabo fugiat hic impedit inventore ipsum laudantium maxime nihil nostrum, numquam officia pariatur porro provident quam quas quibusdam quisquam quos repellat reprehenderit soluta sunt suscipit ullam, voluptatem voluptates? Adipisci hic maiores porro qui quibusdam quidem reiciendis? Animi autem corporis illo iusto laboriosam maiores non pariatur quae velit! Accusamus doloribus expedita fugit, labore nam nemo neque nobis non officiis perspiciatis, qui quos reiciendis rerum sed tempore ullam, voluptatum. Consectetur debitis delectus deserunt dolores et, minus neque nesciunt. Adipisci amet deleniti deserunt earum et eveniet expedita explicabo fuga id illo illum in ipsa ipsum iusto laudantium maiores molestias nam, nostrum placeat quasi qui quia repellendus reprehenderit soluta sunt tempore ut, voluptas. Commodi, mollitia sit? Autem ea enim quasi! Doloribus enim facere nulla quas quis quod similique tempora. A ab aliquid eum harum in ipsam iure laudantium magni minima pariatur possimus quae quaerat quasi quia, similique sunt ullam vero vitae? Eius eligendi et hic itaque neque nesciunt nostrum odio officia placeat porro, quam reprehenderit similique! Cumque cupiditate fugiat fugit illo, incidunt iusto maxime nihil omnis quaerat rerum tenetur vitae voluptas voluptatem! Accusamus asperiores illo labore quasi. A animi asperiores at atque aut beatae consequatur corporis cumque cupiditate delectus deserunt distinctio dolorem dolorum eos error et facilis, fugit, harum id in incidunt magni minus natus neque numquam odio perferendis possimus quaerat quo quod sed sequi tempora tenetur ut veritatis vero voluptatibus. Assumenda iure laboriosam laborum necessitatibus vero voluptatibus. Aliquam cumque, eos est et, fugit ipsam iure, laborum maxime porro provident quasi quos similique voluptas. Accusantium alias aliquam assumenda atque consectetur consequuntur cupiditate deserunt dicta dolores eos laboriosam maiores, maxime mollitia neque numquam pariatur perferendis possimus quaerat quam repellendus repudiandae sint sit suscipit vel, vero voluptatum.
                      </div>
                    </CustomScrollbars>
                  </div>
                </div>
                <div
                  key='checkbox'
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-8-16 col-flex-xl-5-16'
                >
                  <Checkbox
                    label='Госпитализации в прошлом'
                    value={ hospitalized }
                    onChange={ this.handleChangeValue('hospitalized') }
                  />
                </div>
              </div>
              <div key='row4' className='row-flex-wrap-start-start'>
                <div
                  key='testCustomTable'
                  style={{ height: 200 }}
                  className='form-item-box col-flex-16-16 col-flex-sm-16-16 col-flex-md-16-16 col-flex-xl-8-16'
                >
                  <div key='testTable' style={{ width: '100%', height: 200 }}>
                    <CustomTable
                      columns={ table.columns }
                      rows={ table.rows }
                      emptyContent={ 'No data to display' }
                      cbColumnsChanged={ this.cbCustomTable_columnsChanged }
                      cbRowDoubleClicked={ this.cbCustomTable_rowDoubleClicked }
                    />
                  </div>
                </div>
              </div>
            </div>
          </CustomScrollbars>
        </div>
      </div>
    )
  }
}

export default MainPageMainSection;
