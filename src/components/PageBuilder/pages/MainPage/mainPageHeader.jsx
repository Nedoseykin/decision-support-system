import React from 'react';

export const mainPageHeader = () => {
  return (
    <div className='page--header'>
      <h1>Main page header</h1>
    </div>
  )
};
