import React from 'react';
import Button from '../../../../../../buttons/BasicButton/BasicButton';

const EmptyContent = props => {
  const {
    label,
    onClick,
  } = props;

  return (
    <Button
      label={ label }
      size='medium'
      variant='text'
      btnType='primary'
      onClick={ onClick }
    />
  )
};

export default EmptyContent;
