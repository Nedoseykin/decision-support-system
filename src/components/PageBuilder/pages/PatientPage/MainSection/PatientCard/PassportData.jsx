import React from 'react';
import Input from '../../../../../controls/TextInput/TextInput';
import DateInput from '../../../../../controls/DateInput/DateInput';
import RadioGroup from '../../../../../controls/RadioInput/RadioInput';
import Scrollbars from '../../../../../CustomScrollbars/CustomScrollbars';

import { SEX } from '../../../../../../consts/common_consts';

const { MAIL, FEMAIL } = SEX;

const PassportData = props => {
  const {
    firstName           = '',
    middleName          = '',
    lastName            = '',
    address             = '',
    mainDiagnosis       = '',
    concurrentDiagnosis = '',
    creationDate        = new Date().getTime(),
    birthDate           = null,
    sex                 = MAIL,
    age                 = '18',
    phone               = '',
    cbChanged           = null,
  } = props;

  const handleChange = name => value => {
    !!cbChanged && cbChanged(name, value);
  };

  return (
    <Scrollbars>
      <div key='creationDate' className='patient-card--form-control'>
        <DateInput
          label='Дата заполнения'
          value={ creationDate }
          onChange={ handleChange('creationDate') }
        />
      </div>
      <div key='firstName' className='patient-card--form-control'>
        <Input
          label='Имя'
          value={ firstName }
          onChange={ handleChange('firstName') }
        />
      </div>
      <div key='middleName' className='patient-card--form-control'>
        <Input
          label='Отчество'
          value={ middleName }
          onChange={ handleChange('middleName') }
        />
      </div>
      <div key='lastName' className='patient-card--form-control'>
        <Input
          label='Фамилия'
          value={ lastName }
          onChange={ handleChange('lastName') }
        />
      </div>
      <div key='birthday' className='patient-card--form-control'>
        <DateInput
          label='Дата рождения'
          value={ birthDate }
          onChange={ handleChange('birthDate') }
        />
      </div>
      <div key='age' className='patient-card--form-control'>
        <Input
          label='Возраст'
          value={ `${ age }` }
          onChange={ handleChange('age') }
        />
      </div>
      <div key='sex' className='patient-card--form-control'>
        <RadioGroup
          label='Пол'
          value={ sex }
          itemsList={ [ { id: MAIL, label: 'Мужской' }, { id: FEMAIL, label: 'Женский' } ] }
          onChange={ handleChange('sex') }
        />
      </div>
      <div key='address' className='patient-card--form-control'>
        <Input
          label='Адрес'
          value={ address }
          onChange={ handleChange('address') }
        />
      </div>
      <div key='phone' className='patient-card--form-control'>
        <Input
          label='Телефон'
          value={ phone }
          onChange={ handleChange('phone') }
        />
      </div>
      <div key='mainDiagnosis' className='patient-card--form-control'>
        <Input
          label='Основной диагноз'
          value={ mainDiagnosis }
          onChange={ handleChange('mainDiagnosis') }
        />
      </div>
      <div key='concurrentDiagnosis' className='patient-card--form-control'>
        <Input
          label='Сопутствующий диагноз'
          value={ concurrentDiagnosis }
          onChange={ handleChange('concurrentDiagnosis') }
        />
      </div>
    </Scrollbars>
  );
};

export default PassportData;
