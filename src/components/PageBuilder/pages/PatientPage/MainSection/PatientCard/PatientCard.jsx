import React from 'react';
import './PatientCard.scss';

import Tabs from '../../../../../Tabs/Tabs';
import Tab from '../../../../../Tabs/Tab/Tab';
// import CustomScrollbars from '../../../../../CustomScrollbars/CustomScrollbars';

import { COLORS } from '../../../../../../consts/theme_consts';
import { PATIENT_CARD_TABS } from '../../../../../../consts/patient_card/patient_card_tabs';
// import { updateTabState } from '../../../../../../utils/ui/table_ui_utils';
import { findArrayItem } from '../../../../../../utils/utils';

const {
  TAB_BACKGROUND_COLOR,
  ACTIVE_TAB_BACKGROUND_COLOR,
} = COLORS;

const PatientCard = props => {
  const {
    activeTabId,
    tabs: tabsList,
    cbTabChanged         = null,
    cbPatientDataChanged = null,
  } = props;

  const activeTab = findArrayItem(tabsList, { id: activeTabId });
  const { component: ActiveTab, state: tabState } = activeTab;
  // console.log('activeTab: ', activeTab);

  const handleTabChange = (event, value) => {
    !!cbTabChanged && cbTabChanged(value);
  };

  const handleChangePatientData = (name, value) => {
    // updateTabState(activeTabId, { [ name ]: value });
    !!cbPatientDataChanged && cbPatientDataChanged({ [ name ]: value });
  };

  const tabsProps = () => {
    return {
      value    : activeTabId,
      classes  : {
        root: {
          height: 32,
          backgroundColor: TAB_BACKGROUND_COLOR,
          borderRadius: '8px 8px 0 0',
        },
        scrollableArea: {
          height: '100%',
        }
      },
      onChange : handleTabChange,
    }
  };

  const tabs = () => {
    return PATIENT_CARD_TABS.map(tab => {
      const backgroundColor = tab.id === activeTab.id
        ? ACTIVE_TAB_BACKGROUND_COLOR
        : TAB_BACKGROUND_COLOR;
      return (
        <Tab
          key={ tab.id }
          classes={{
            root: {
              backgroundColor,
              width: 200,

            },
          }}
          label={ tab.label }
          value={ tab.id }
        />
      )
    });
  };

  return (
    <div className='patient-card'>
      <div key='tabulatorBox' className='patient-card--tabulator-box'>
        <Tabs { ...tabsProps() }>
          { tabs() }
        </Tabs>
      </div>
      <div key='tabContentBox' className='patient-card--tab-content-box'>
        <ActiveTab
          { ...tabState }
          cbChanged={ handleChangePatientData }
        />
      </div>
      <div key='tabToolbar' className='patient-card--tab-toolbar'>
        Tab toolbar
      </div>
    </div>
  )
};

export default PatientCard;
