import './Procedures.scss';
import React from 'react';
import DynamicTable from '../../../../../CustomTable/CustomTable';
import EmptyContent from './Common/EmptyContent';
import { findArrayItemIndex, isNotEmptyArray } from '../../../../../../utils/utils';
import {
  calculateRows,
  getMonthCountToMYStr,
  getTabState,
  updateTabState
} from '../../../../../../utils/ui/table_ui_utils';
import Button from '../../../../../buttons/BasicButton/BasicButton';
import AddIcon from '../../../../../icons/IconAdd';
import { THEME } from '../../../../../../consts/style_const/theme';
import DeleteIcon from '../../../../../icons/IconDelete';
import { PATIENT_CARD_TAB_ID } from '../../../../../../consts/patient_card/patient_card_tab_id';

const { PROCEDURES } = PATIENT_CARD_TAB_ID;

const Procedures = props => {
  const {
    columns = [],
    rows = [],
    cbChanged,
  } = props;

  const handleColumnsChanged = cols => {
    !!cbChanged && cbChanged('columns', cols);
  };

  const handleAddRowClick = rowId => event => {
    // console.log('Add row to row: ', rowId);
    const tabState = getTabState(PROCEDURES);
    let { rows } = tabState;
    // console.log('rows: ', rows);
    let visitDate = 0;
    let newId = 1;
    if (isNotEmptyArray(rows)) {
      rows.forEach(r => {
        newId = Math.max(r.id, newId);
        if (r.id === rowId) visitDate = r.visitDate + 1;
      });
      newId++;
    }
    rows = rows.slice();
    rows.push({
      id: newId,
      visitDate,
      ablation: 'Ablation',
      sma: 'sma',
      delta: 0,
    });
    updateTabState(PROCEDURES, { rows });
  };

  const handleDeleteRowClick = rowId => event => {
    // console.log('Delete row to row: ', rowId);
    const i = findArrayItemIndex(rows, { id: rowId });
    const newRows = rows.slice();
    newRows.splice(i, 1);
    updateTabState(PROCEDURES, { rows: newRows });
  };

  const getEmptyContent = () => {
    return (
      <EmptyContent
        label='Новая процедура'
        onClick={ handleAddRowClick(-1) }
      />
    )
  };

  const getRows = (cols) => {
    const VALUE_MAKERS = {
      id: {},
      visitDate: {
        // tdValueMaker: getMonthCountToMYStr,
      },
      ablation: {
        // tdValueMaker: v => `${ v.length } x ${ v.width }`,
      },
      sma: {},
      delta: {},
      buttons: {
        tdValueMaker: v => {
          return (
            [
              <Button
                key='addBtn'
                fullWidth={ false }
                disabled={ null /*getAddBtnDisabledStatus(v)*/ }
                classes={{
                  root: 'root--row-btn',
                  label: 'label--special',
                  content: 'content--special'
                }}
                size='medium'
                variant='filled'
                btnType='primary'
                onClick={ handleAddRowClick(v) }
              >
                <AddIcon
                  width={ 16 }
                  height={ 16 }
                  stroke={ THEME.colors.contrastWhite }
                />
              </Button>,
              <Button
                key='deleteBtn'
                fullWidth={ false }
                disabled={ null /*getDeleteBtnDisabledStatus(v)*/ }
                classes={{
                  root: 'root--row-btn',
                  label: 'label--special',
                  content: 'content--special'
                }}
                size='medium'
                variant='filled'
                btnType='secondary'
                onClick={ handleDeleteRowClick(v) }
              >
                <DeleteIcon
                  width={ 16 }
                  height={ 16 }
                  stroke={ THEME.colors.contrastWhite }
                />
              </Button>
            ]
          )},
      },
    };

    cols = cols.map(col => {
      const { id } = col;
      const { tdValueMaker } = VALUE_MAKERS[ id ];
      !!tdValueMaker && (col.tdValueMaker = tdValueMaker);
      return { ...col };
    });

    const sortedRows = isNotEmptyArray(rows)
      ? rows.sort((r1, r2) => r1.visitDate - r2.visitDate)
      : rows;

    return calculateRows(sortedRows, cols);
  };

  return (
    <DynamicTable
      className='Procedures'
      emptyContent={ getEmptyContent() }
      columns={ isNotEmptyArray(rows) ? columns : [] }
      rows={ getRows(columns) }
      cbColumnsChanged={ handleColumnsChanged }
    />
  )

};

export default Procedures;
