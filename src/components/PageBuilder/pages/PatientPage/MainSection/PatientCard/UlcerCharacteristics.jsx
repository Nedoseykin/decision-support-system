import './UlcerCharacteristics.scss';
import React from 'react';
import Checkbox from '../../../../../controls/Checkbox/Checkbox';
import Section from '../../../../../Section/Section';
import TextInput from '../../../../../controls/TextInput/TextInput';
import Scrollbars from '../../../../../CustomScrollbars/CustomScrollbars';

import { ETHIOLOGY_ITEMS } from '../../../../../../consts/common_consts';
import { isExists } from '../../../../../../utils/utils';

const UlcerCharacteristics = props => {
  const {
    ulcerEthiology         = 0,
    ulcerAnamnesisDuration = 0,
    lastPersistDuration    = 0,
    previousOperations     = '',
    previousTreatment      = '',
    ulcerLocalization      = '',
    cbChanged,
  } = props;

  const handleEthiologyChange = bit => () => {
    !!cbChanged && cbChanged('ulcerEthiology', (ulcerEthiology ^ bit));
  };

  const handleChange = name => value => {
    !!cbChanged && cbChanged(name, value);
  };

  const handleChangeYears = name => value => {
    console.log('handleChangeYears: name: ', name, ', value: ', value);
    let year = parseInt(value, 10);
    if (!isFinite(year) || isNaN(year)) year = 0;
    if (year > 150) year = 150;
    if (year < 0) year = 0;
    let oldDuration = props[ name ];
    let month = 0;
    if (isExists(oldDuration) && isFinite(oldDuration) && !isNaN(oldDuration)) {
      month = oldDuration % 12;
      console.log('month: ', month);
    }
    const result = year * 12 + month;
    console.log('result: ', result);

    handleChange(name)(result);
  };

  const handleChangeMonths = name => value => {
    console.log('handleChangeMonths: name: ', name, ', month value: ', value);
    let month = parseInt(value, 10);
    if (!isFinite(month) || isNaN(month)) month = 0;
    if (month > 11) month = 11;
    if (month < 0) month = 0;
    console.log('month: ', month);
    let oldDuration = props[ name ];
    console.log('oldDuration: ', oldDuration);
    let year = 0;
    if (isExists(oldDuration) && isFinite(oldDuration) && !isNaN(oldDuration)) {
      year = (oldDuration - oldDuration % 12) / 12;
      console.log('year: ', year);
    }
    const result = year * 12 + month;
    console.log('result: ', result);

    handleChange(name)(result);
  };

  const getYears = value => {
    let result = parseInt(value, 10);
    if (!isFinite(result) || isNaN(result)) {
      result = 0;
    }
    else {
      result = (value - value % 12) / 12;
    }
    return `${ result }`;
  };

  const getMonths = value => {
    let result = parseInt(value, 10);
    if (!isFinite(result) || isNaN(result)) {
      result = 0;
    }
    else {
      result = value % 12;
    }
    return `${ result }`;
  };

  const ethiologyItems = ETHIOLOGY_ITEMS
    .map(item => {
      const { id, label, bit } = item;
      return (
        <div key={ id } className='ulcer-characteristic--form-control ethiology-item'>
          <Checkbox
            withLabel={ true }
            label={ label }
            value={ !!(ulcerEthiology & bit) }
            onChange={ handleEthiologyChange(bit) }
          />
        </div>
      )
    });

  return (
    <Scrollbars>
      <Section key='ethiology' label='Этиология язвы' className='ulcer-ethiology'>
        { ethiologyItems }
      </Section>
      <Section key='anamnesis' label='Язвенный анамнез (лет/месяцев)' className='ulcer-anamnesis'>
        <TextInput
          key='years'
          className='years-input'
          withLabel={ false }
          value={ getYears(ulcerAnamnesisDuration) }
          onChange={ handleChangeYears('ulcerAnamnesisDuration') }
        />
        <span className='duration-divider'>/</span>
        <TextInput
          key='months'
          className='months-input'
          withLabel={ false }
          value={ getMonths(ulcerAnamnesisDuration) }
          onChange={ handleChangeMonths('ulcerAnamnesisDuration') }
        />
      </Section>
      <Section key='persistance' label='Длительность последнего незаживления (лет/месяцев)' className='ulcer-persistance'>
        <TextInput
          key='years'
          className='years-input'
          withLabel={ false }
          value={ getYears(lastPersistDuration) }
          onChange={ handleChangeYears('lastPersistDuration') }
        />
        <span className='duration-divider'>/</span>
        <TextInput
          key='months'
          className='months-input'
          withLabel={ false }
          value={ getMonths(lastPersistDuration) }
          onChange={ handleChangeMonths('lastPersistDuration') }
        />
      </Section>
      <Section key='treatment' label='Лечение' className='previous-treatment'>
        <TextInput
          key='surgery'
          withLabel={ true }
          label='Предшествующие операции'
          value={ previousOperations }
          onChange={ handleChange('previousOperations') }
        />
        <TextInput
          key='therapy'
          withLabel={ true }
          label='Предшествующее лечение'
          value={ previousTreatment }
          onChange={ handleChange('previousTreatment') }
        />
      </Section>
      <Section key='localization' label='Локализация язвы' className='ulcer-localization'>
        <TextInput
          withLabel={ true }
          label='Локализация'
          value={ ulcerLocalization }
          onChange={ handleChange('ulcerLocalization') }
        />
      </Section>
    </Scrollbars>
  )

};

export default UlcerCharacteristics;
