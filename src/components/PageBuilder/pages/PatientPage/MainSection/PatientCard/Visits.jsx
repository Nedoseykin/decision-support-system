import './Visits.scss';
import React from 'react';
import DynamicTable from '../../../../../CustomTable/CustomTable';
import Button from '../../../../../buttons/BasicButton/BasicButton';
import AddIcon from '../../../../../icons/IconAdd';
import DeleteIcon from '../../../../../icons/IconDelete';
import { findArrayItemIndex, isNotEmptyArray } from '../../../../../../utils/utils';
import {
  calculateRows,
  getMonthCountToMYStr,
  getTabState,
  updateTabState
} from '../../../../../../utils/ui/table_ui_utils';
import { THEME } from '../../../../../../consts/style_const/theme';
import { PATIENT_CARD_TAB_ID } from '../../../../../../consts/patient_card/patient_card_tab_id';
import EmptyContent from './Common/EmptyContent';

const { VISITS } = PATIENT_CARD_TAB_ID;

const Visits = props => {
  const {
    columns,
    rows,
    cbChanged,
  } = props;

  const handleColumnsChanged = cols => {
    // console.log('columns changed: ', cols);
    !!cbChanged && cbChanged('columns', cols);
  };

  const handleAddRowClick = rowId => event => {
    // console.log('Add row to row: ', rowId);
    const tabState = getTabState(VISITS);
    let { rows } = tabState;
    // console.log('rows: ', rows);
    let name = 0;
    let newId = 1;
    if (isNotEmptyArray(rows)) {
      rows.forEach(r => {
        newId = Math.max(r.id, newId);
        if (r.id === rowId) name = r.name + 1;
      });
      newId++;
      // const row = findArrayItem(rows, { id: rowId });
      // console.log('row: ', row);
      // if (row) name = row.name + 1;
    }
    rows = rows.slice();
    rows.push({
      id: newId,
      name,
      size: { length: 0, width: 0 },
      square: 0,
      delta: 0,
    });
    updateTabState(VISITS, { rows });
  };

  const handleDeleteRowClick = rowId => event => {
    // console.log('Delete row to row: ', rowId);
    const i = findArrayItemIndex(rows, { id: rowId });
    const newRows = rows.slice();
    newRows.splice(i, 1);
    updateTabState(VISITS, { rows: newRows });
  };

  // const columns = () => {
  //   return [
  //     {
  //       id: 'id',
  //       value: 'ID',
  //       columnName: 'ID',
  //       visible: false,
  //       fixed: true,
  //       resizable: true,
  //       movable: false,
  //       width: 50,
  //     },
  //     {
  //       id: 'name',
  //       value: 'Визит',
  //       columnName: 'Визит',
  //       visible: true,
  //       fixed: true,
  //       resizable: true,
  //       movable: false,
  //       width: 100,
  //     },
  //     {
  //       id: 'size',
  //       value: 'Размеры',
  //       columnName: 'Размеры',
  //       visible: true,
  //       fixed: false,
  //       resizable: true,
  //       movable: false,
  //       width: 100,
  //     },
  //     {
  //       id: 'square',
  //       value: 'Площадь',
  //       columnName: 'Площадь',
  //       visible: true,
  //       fixed: false,
  //       resizable: true,
  //       movable: false,
  //       width: 100,
  //     },
  //     {
  //       id: 'delta',
  //       value: '+/- (%)',
  //       columnName: '+/- (%)',
  //       visible: true,
  //       fixed: false,
  //       resizable: true,
  //       movable: false,
  //       width: 100,
  //     },
  //   ]
  // };

  const getAddBtnDisabledStatus = rowId => {
    const i = findArrayItemIndex(rows, { id: rowId });
    const { name } = rows[ i ];
    if (i < rows.length - 1) {
      const nextRow = rows[ i + 1 ];
      const { name: nextName } = nextRow;
      if (nextName - name < 2) return true;
    }
    return false;
  };

  const getDeleteBtnDisabledStatus = rowId => {
    const i = findArrayItemIndex(rows, { id: rowId });
    return (i === 0 && rows.length > 1);
  };

  const getEmptyContent = () => {
    return (
      <EmptyContent
        label='Новое посещение'
        onClick={ handleAddRowClick(-1) }
      />
    );
  };

  const getRows = cols => {
    const VALUE_MAKERS = {
      id: {},
      name: {
        tdValueMaker: getMonthCountToMYStr,
      },
      size: {
        tdValueMaker: v => `${ v.length } x ${ v.width }`,
      },
      square: {},
      delta: {},
      buttons: {
        tdValueMaker: v => {
          return (
            [
              <Button
                key='addBtn'
                fullWidth={ false }
                disabled={ getAddBtnDisabledStatus(v) }
                classes={{
                  root: 'root--row-btn',
                  label: 'label--special',
                  content: 'content--special'
                }}
                size='medium'
                variant='filled'
                btnType='primary'
                onClick={ handleAddRowClick(v) }
              >
                <AddIcon
                  width={ 16 }
                  height={ 16 }
                  stroke={ THEME.colors.contrastWhite }
                />
              </Button>,
              <Button
                key='deleteBtn'
                fullWidth={ false }
                disabled={ getDeleteBtnDisabledStatus(v) }
                classes={{
                  root: 'root--row-btn',
                  label: 'label--special',
                  content: 'content--special'
                }}
                size='medium'
                variant='filled'
                btnType='secondary'
                onClick={ handleDeleteRowClick(v) }
              >
                <DeleteIcon
                  width={ 16 }
                  height={ 16 }
                  stroke={ THEME.colors.contrastWhite }
                />
              </Button>
            ]
        )},
      },
    };

    cols = cols.map(col => {
      const { id } = col;
      const { tdValueMaker } = VALUE_MAKERS[ id ];
      !!tdValueMaker && (col.tdValueMaker = tdValueMaker);
      return { ...col };
    });

    const sortedRows = isNotEmptyArray(rows)
      ? rows.sort((r1, r2) => r1.name - r2.name)
      : rows;

    return calculateRows(sortedRows, cols);
  };

  return (
    <DynamicTable
      className='Visits'
      columns={ isNotEmptyArray(rows) ? columns : [] }
      rows={ getRows(columns) }
      emptyContent={ getEmptyContent() }
      cbColumnsChanged={ handleColumnsChanged }
    />
  )
};

export default Visits;
