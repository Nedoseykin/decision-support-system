import './PatientPageMainSection.scss';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import PatientCard from './PatientCard/PatientCard';
import { acUISetActiveTab } from '../../../../../store/actions';
import { updateTabState } from '../../../../../utils/ui/table_ui_utils';

const mapStateToProps = ({ ui }) => ({
  activeTabId : ui.patientCardActiveTabId,
  tabs        : ui.patientCardTabs,
});

@connect(mapStateToProps)
class PatientPageMainSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  pcard_cbTabChanged = activeTabId => {
    // console.log('activeTabId: ', activeTabId);
    this.props.dispatch(acUISetActiveTab({ activeTabId }));
  };

  pcard_cbPatientDataChanged = patientData => {
    // this.setState({ ...patientData });
    const { activeTabId } = this.props;
    updateTabState(activeTabId, patientData);
  };

  render() {
    const { activeTabId, tabs } = this.props;
    return (
      <div className='page--main-section'>
        <h2 className='main-section--title'>Карта пациента</h2>
        <hr/>
        <div className='main-section--central-panel'>
          <PatientCard
            activeTabId={ activeTabId }
            tabs={ tabs }
            patientData = {{ ...this.state }}
            cbTabChanged={ this.pcard_cbTabChanged }
            cbPatientDataChanged = { this.pcard_cbPatientDataChanged }
          />
        </div>
      </div>
    )
  }
}

export default PatientPageMainSection;
