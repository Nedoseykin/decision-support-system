import { mainPageHeader } from './MainPage/mainPageHeader';
import { mainPageMainSection } from './MainPage/mainPageMainSection';
import { mainPageToolbar } from './MainPage/mainPageToolbar';

import { patientPageHeader } from './PatientPage/patientPageHeader';
import { patientPageToolbar } from './PatientPage/patientPageToolbar';
import { patientPageMainSection } from './PatientPage/patientPageMainSection';

import { PAGES_ID } from './pagesId';

const { MAIN_PAGE, PATIENT_PAGE, LOGIN_PAGE, ERROR_PAGE } = PAGES_ID;

export const PAGES_CONFIG = {
  [ MAIN_PAGE ]: {
    pageBuilderProps: {
      className: 'MainPage',
      header: mainPageHeader,
      mainSection: mainPageMainSection,
      toolbar: mainPageToolbar,
    },
  },
  [ PATIENT_PAGE ]: {
    pageBuilderProps: {
      className: 'PatientPage',
      header: patientPageHeader,
      mainSection: patientPageMainSection,
      toolbar: patientPageToolbar,
    }
  },
  [ LOGIN_PAGE ]: {
    pageBuilderProps: {
      className: 'LoginPage',
      header: null,
      mainSection: null,
      toolbar: null,
    },
  },
  [ ERROR_PAGE ]: {
    pageBuilderProps: {
      className: 'ErrorPage',
      header: null,
      mainSection: null,
      toolbar: null,
    },
  },
};
