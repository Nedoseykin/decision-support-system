import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const mapStateToProps = ( state, ownProps ) => ({
  isLogged: state.auth.isLogged,

  routeProps: {
    exact            : ownProps.exact,
    path             : ownProps.path,
    component        : ownProps.component,
    render           : ownProps.render,
  },
});

@connect( mapStateToProps )
class PrivateRoute extends Component {

  render() {
    const {
      isLogged,
      routeProps,
      location,
    } = this.props;
    // console.log('props: ', this.props);
    return isLogged
      ? (
          <Route { ...routeProps } />
        )
      : (
          <Redirect to={ {
            pathname: '/login',
            state: { from: location.pathname }
          } } />
        );
  }
}

export default PrivateRoute;
