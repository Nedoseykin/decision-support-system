import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import PrivateRoute from './PrivateRoute';
import { history } from '../../store/configureStore';

import PageBuilder from '../PageBuilder/PageBuilder';

import { PAGES_ID } from '../PageBuilder/pages/pagesId';
import { PAGES_CONFIG } from '../PageBuilder/pages/pagesConfig';

const { MAIN_PAGE, PATIENT_PAGE, LOGIN_PAGE, ERROR_PAGE, } = PAGES_ID;

class Router extends Component {
  render() {
    return (
      <ConnectedRouter history={ history }>
        <Switch>
          <PrivateRoute
            exact path='/'
            render={ () => <PageBuilder { ...PAGES_CONFIG[ MAIN_PAGE ].pageBuilderProps } /> }
          />
          <PrivateRoute
            exact path='/patients/new'
            render={ () => <PageBuilder { ...PAGES_CONFIG[ PATIENT_PAGE ].pageBuilderProps } /> }
          />
          <Route
            exact
            path='/login'
            render={ () => <PageBuilder { ...PAGES_CONFIG[ LOGIN_PAGE ].pageBuilderProps } /> }
          />
          <Route
            render={ () => <PageBuilder { ...PAGES_CONFIG[ ERROR_PAGE ].pageBuilderProps } /> }
          />
        </Switch>
      </ConnectedRouter>
    );
  }
}

export default Router;
