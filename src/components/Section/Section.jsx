import './Section.scss';
import React from 'react';
import classNames from 'classnames';
import { isNotEmpty } from '../../utils/utils';

const Section = props => {
  const {
    label,
    children,
    className,
  } = props;
  return (
    <div className={ classNames('Section', isNotEmpty(className) && className) }>
      <div key='label' className='section-label'>
        { label }
      </div>
      <div key='content' className='section-content'>
        { children }
      </div>
    </div>
  )
};

export default Section;
