import './Tab.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isNotEmpty } from '../../../utils/utils';

class Tab extends PureComponent {
  // hidden prop onClick is received from Tabs ( parent )

  static propTypes = {
    classes: PropTypes.object, // { iconRoot,  }
    className: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.oneOfType( [
      PropTypes.string, PropTypes.number
    ] ).isRequired,
    icon: PropTypes.func,
    withIcon: PropTypes.bool,
    onIconClick: PropTypes.func,
  };

  static defaultProps = {
    classes: {},
    className: null,
    disabled: false,
    label: '',
    icon: null,
    withIcon: false,
    onIconClick: null,
  };

  classCSS = 'Tab';

  handleIconClick = event => {
    event.stopPropagation();
    event.preventDefault();
    const { value, onIconClick } = this.props;
    !!onIconClick && onIconClick( value );
  };

  handleClick = event => {
    event.stopPropagation();
    event.preventDefault();
    const { onClick, value } = this.props;
    !!onClick && onClick( event, value );
  };

  render() {
    const {
      classes,
      className,
      disabled,
      label,
      icon,
      withIcon,
      selected,
    } = this.props;
    const {
      root,
      labelBox,
      iconBox,
      iconRoot,
    } = classes;
    const Icon = !!icon ? icon : null;
    return (
      <div
        className = { classNames(
          this.classCSS,
          isNotEmpty( className ) && className,
          ( !disabled && selected ) && 'selected' )
        }
        style = { !!root ? root : {} }
        data-disabled = { !!disabled }
        onClick = { this.handleClick }
      >
        <span
          key = 'label'
          className = { `${ this.classCSS }-label-box` }
          style = { !!labelBox ? labelBox : {} }
        >
          <span>
            { label }
          </span>
        </span>
        {
          ( withIcon && !!Icon ) &&
            <span
              key = 'icon'
              className = { `${ this.classCSS }-icon-box` }
              style = { !!iconBox ? iconBox : {} }
            >
              <Icon
                onClick = { this.handleIconClick }
                style = { !!iconRoot ? iconRoot : {} }
              />
            </span>
        }
      </div>
    )
  }
}

export default Tab;
