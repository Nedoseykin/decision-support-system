import './Tabs.scss';
import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import { isNotEmpty, isNotEmptyArray } from '../../utils/utils';
import classNames from 'classnames';

class Tabs extends PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType( [ PropTypes.object, PropTypes.arrayOf( PropTypes.object ) ] ),
    classes: PropTypes.object, // { root, scrollableArea, prevIconBox, nextIconBox, }
    className: PropTypes.string,
    value: PropTypes.oneOfType( [
      PropTypes.string, PropTypes.number,
    ] ),
    onChange: PropTypes.func,
    withScrollButtons: PropTypes.bool,
  };

  static defaultProps = {
    children: null,
    classes: {},
    className: null,
    onChange: null,
    withScrollButtons: true,
  };

  componentDidMount() {
    this.prepareStyles();
  }

  componentDidUpdate( prevProps, prevState, snapshot ) {
    this.prepareStyles();
  }

  prepareStyles = () => {
    this.correctScrollPositionForSelectedTab();
  };

  classCSS = 'Tabs';

  handleChange = ( event, value ) => {
    const { onChange } = this.props;
    !!onChange && onChange( event, value );
  };

  handleScrollBtnClick = scrollBtnType => event => {
    event.preventDefault();
    event.stopPropagation();
    if ( !this.scroller ) return null;
    const scrollLeftValue = this.getScrollLeftValueDiff( scrollBtnType );
    this.animateScroll( scrollLeftValue );
  };

  getScrollLeftValueDiff = scrollBtnType => {
    if ( !this.scroller ) return null;
    const scroller = ReactDom.findDOMNode( this.scroller );
    const tabs = this.getTabsHtmlDOMElements();
    if ( !isNotEmptyArray( tabs ) ) return null;
    const {
      clientWidth,
      scrollLeft,
      scrollWidth,
    } = scroller;
    const clientRight = scrollLeft + clientWidth;
    const scrollRight = scrollWidth - clientRight;
    let diff = 0;
    if ( scrollBtnType === 'prev' ) {
      const target = Math.min( clientWidth * 0.9, scrollLeft );
      let newScrollLeft = scrollLeft - target;
      for ( let i = 0; i < tabs.length; i++ ) {
        let { offsetLeft: tabLeft, offsetWidth: tabWidth } = tabs[ i ];
        tabLeft -= newScrollLeft;
        const tabRight = tabLeft + tabWidth;
        if ( ( tabLeft <= 0 ) && ( tabRight >= 0 ) ) {
          diff = tabLeft - target;
          break;
        } else {
          diff = -1 * target;
        }
      }
    } else {
      const target = Math.min( clientWidth * 0.9, scrollRight );
      const newClientRight = clientRight + target;
      for ( let i = tabs.length - 1; i >= 0; i-- ) {
        let { offsetLeft: tabLeft, offsetWidth: tabWidth } = tabs[ i ];
        const tabRight = tabLeft + tabWidth;
        if ( ( tabLeft <= newClientRight ) && ( tabRight >= newClientRight ) ) {
          diff = tabRight - clientRight;
          break;
        } else {
          diff = target;
        }
      }
    }
    return diff;
  };

  correctScrollPositionForSelectedTab = () => {
    const { value, children } = this.props;
    if ( !isNotEmptyArray( children ) ) return null;
    let chValues = children.map( ch => ch.props.value );
    const i = chValues.indexOf( value );
    if ( i === -1 ) return null;
    const tabs = this.getTabsHtmlDOMElements();
    if ( !isNotEmptyArray( tabs ) || ( tabs.length <= value ) ) return null;
    const { clientWidth, scrollLeft } = this.scroller;
    const clientRight = scrollLeft + clientWidth;
    const { offsetLeft: tabOffsetLeft, offsetWidth: tabOffsetWidth } = tabs[ i ];
    const tabOffsetRight = tabOffsetLeft + tabOffsetWidth;
    if ( tabOffsetLeft < scrollLeft ) {
      const diff = tabOffsetLeft - scrollLeft;
      this.animateScroll( diff );
    } else if ( tabOffsetRight > clientRight ) {
      const diff = tabOffsetRight - clientRight;
      this.animateScroll( diff );
    }
  };

  animateScroll = diff => {
    if ( ( diff === 0 ) || !this.scroller ) return null;
    const scroller = ReactDom.findDOMNode( this.scroller );
    let startScrollLeft = scroller.scrollLeft;
    const direction = Math.sign( diff );
    let stepDiff = Math.abs( diff ) / 30;
    let testDiff = Math.abs( diff );
    const loop = () => {
      let d = Math.min( stepDiff, testDiff );
      testDiff -= d;
      startScrollLeft += ( d * direction );
      scroller.scrollLeft = startScrollLeft;
      if ( testDiff > 0 ) requestAnimationFrame( loop );
    };
    requestAnimationFrame( loop );
  };

  getTabsHtmlDOMElements = () => {
    if ( !this.scroller ) return null;
    const scroller = ReactDom.findDOMNode( this.scroller );
    if ( !scroller ) return null;
    let tabs = scroller.querySelectorAll( '.Tab' );
    if ( !isNotEmpty( tabs ) ) return null;
    tabs = Array.prototype.map.call( tabs, tab => tab );
    return tabs;
  };

  getItems = children => {
    if ( !isNotEmptyArray( children ) ) return null;
    const { value } = this.props;
    return children.map( c => {
      return {
        ...c,
        props: {
          ...c.props,
          selected: c.props.value === value,
          onClick: this.handleChange,
        }
      };
    } );
  };

  renderScrollIcon = iconType => {
    let d = '';
    switch ( iconType ) {
      case 'prev':
        d = 'M 14 2 L 6 10 L 14 18';
      break;
      case 'next':
        d = 'M 6 2 L 14 10 L 6 18';
      break;
      default:
    }
    return (
      <svg
        xmlns = 'http://www.w3.org/2000/svg'
        width = '0.8em'
        height = '0.8em'
        viewBox = '0 0 20 20'
        onMouseDown = { this.handleScrollBtnClick( iconType ) }
      >
        {
          isNotEmpty( d ) &&
          <path
            d = { d }
            strokeWidth = { 3 }
            strokeLinejoin = 'round'
          />
        }
      </svg>
    )
  };

  render() {
    const {
      children,
      classes,
      className,
      withScrollButtons,
    } = this.props;
    const {
      root,
      scrollableArea,
      prevIconBox,
      nextIconBox,
    } = classes;
    const prevIcon = this.renderScrollIcon( 'prev' );
    const nextIcon = this.renderScrollIcon( 'next' );
    const items = this.getItems( children );
    return (
      <div
        className = { classNames( `${ this.classCSS }`, !!className && className ) }
        style = { !!root ? root : {} }
      >
        {
          withScrollButtons &&
          <div
            key = 'prevBox'
            className = { `${ this.classCSS }-scroll-icon-box` }
            style = { !!prevIconBox ? prevIconBox : {} }
          >
            { prevIcon }
          </div>
        }
        <div
          key = 'scrollableArea'
          className = { `${ this.classCSS }-scrollable-area` }
          style = { !!scrollableArea ? scrollableArea : {} }
        >
          <div
            key = 'scroller'
            className = { `${ this.classCSS }-scroller` }
            ref = { elm => { this.scroller = elm; } }
          >
            { items }
          </div>
        </div>
        {
          withScrollButtons &&
          <div
            key = 'nextBox'
            className = { `${ this.classCSS }-scroll-icon-box` }
            style = { !!nextIconBox ? nextIconBox : {} }
          >
            { nextIcon }
          </div>
        }
      </div>
    )
  }
}

export default Tabs;
