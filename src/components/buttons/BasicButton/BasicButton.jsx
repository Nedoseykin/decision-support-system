import './BasicButton.scss';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function BasicButton(props) {
  const classCSS = 'BasicButton';
  const {
    btnType,
    classes,
    label,
    children,
    disabled,
    fullWidth,
    onClick,
    size,
    variant,
  } = props;

  const className = getClassName({ className: classCSS, disabled, fullWidth, size, variant, btnType });
  const rootClassName = getClassNameFromClasses(classes, 'root');

  const labelClassName = getClassNameFromClasses(classes, 'label');
  const contentClassName = getClassNameFromClasses(classes, 'content');

  return (
    <div
      className={ classNames(className, !!rootClassName && rootClassName) }
      onClick={ !!onClick && !disabled ? onClick : null }
    >
      { !!label && <Label { ...{ classCSS, label, disabled, labelClassName } } /> }
      { !!children && <Content { ...{ classCSS, children, disabled, contentClassName } }/> }
    </div>
  )
}

BasicButton.propTypes = {
  classes   : PropTypes.object,
  disabled  : PropTypes.bool,
  fullWidth : PropTypes.bool,
  label     : PropTypes.string,
  onClick   : PropTypes.func,
  size      : PropTypes.oneOf([ 'small', 'medium', 'large' ]),
  variant   : PropTypes.oneOf([ 'text', 'outlined', 'filled' ]),
  btnType   : PropTypes.oneOf([ 'default', 'primary', 'secondary' ]),
};

BasicButton.defaultProps = {
  classes   : {},
  disabled  : false,
  fullWidth : false,
  onClick   : null,
  size      : 'medium',
  variant   : 'text',
  btnType   : 'primary',
};

function Label({ label, classCSS, labelClassName, disabled }) {
  const className = getClassName({className: `${ classCSS }--label`, disabled});
  return (
    <span
      className={ classNames(className, !!labelClassName && labelClassName) }
    >
      { label }
    </span>
  )
}

function Content({ children, classCSS, contentClassName, disabled }) {
  const className = getClassName({ className: `${ classCSS }--content`, disabled });
  return (
    <span className={ classNames(className, !!contentClassName && contentClassName) }>
      { children }
    </span>
  )
}

function getClassNameFromClasses (classes, key) {
  let result = null;
  if (!!classes && classes[ key ]) result = classes[ key ];
  return result;
}

function getClassName({ className, disabled, fullWidth, size, variant, btnType }) {
  return classNames(
    className,
    fullWidth && `${ className }--full-width`,
    !!size && `${ className }--${ size }`,
    !!variant && `${ className }--${ variant }`,
    !!btnType && `${ className }--${ btnType }`,
    disabled && `${ className }--disabled`
  );
}
