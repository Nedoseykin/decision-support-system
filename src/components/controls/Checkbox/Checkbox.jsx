import './Checkbox.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Label from '../Label/Label';
import CheckedIcon from '../../icons/IconCheckboxChecked';
import UncheckedIcon from '../../icons/IconCheckboxUnchecked';

import { isNotEmpty } from '../../../utils/utils';
import { COLORS } from '../../../consts/theme_consts';

const focusColor = COLORS.FOCUS_COLOR;
const blurColor = COLORS.BLUR_COLOR;

class Checkbox extends PureComponent {
  static classId = 0;

  static getId = htmlId => isNotEmpty(htmlId)
    ? htmlId
    : `Checkbox_${ Checkbox.classId }`;

  static propTypes = {
    className: PropTypes.string,
    withLabel: PropTypes.bool,
    withValidation: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.bool,
    iconSize: PropTypes.number,
    validationMessage: PropTypes.string,
    iconChecked: PropTypes.oneOfType([ PropTypes.func, PropTypes.object ]),
    iconUnchecked: PropTypes.oneOfType([ PropTypes.func, PropTypes.object ]),
    onChange: PropTypes.func,
  };

  static defaultProps = {
    className: null,
    withLabel: true,
    withValidation: false,
    label: null,
    value: false,
    iconSize: 16,
    validationMessage: null,
    iconChecked: CheckedIcon,
    iconUnchecked: UncheckedIcon,
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    Checkbox.classId++;
    this.classCSS = 'Checkbox';
    this.state = {
      htmlId: Checkbox.getId(props.htmlId),
    };
  }

  handleChange = event => {
    event.preventDefault();
    event.stopPropagation();
    const { onChange, value } = this.props;
    !!onChange && onChange( !value );
  };

  handleOnFocus = () => {
    !!this.label && (this.label.style.color = focusColor);
    // this.input.style.borderBottomColor = focusColor;
  };

  handleOnBlur = () => {
    !!this.label && (this.label.style.color = blurColor);
    // this.input.style.borderBottomColor = blurColor;
  };

  getLabelRef = ref => { this.label = ref; };

  getInput = () => {
    const { value, iconChecked, iconUnchecked, iconSize } = this.props;
    const { htmlId } = this.state;
    const StatusIcon = value ? iconChecked : iconUnchecked;
    return (
      <div
        key='inputBox'
        tabIndex={ 0 }
        className={ classNames('checkbox--input-box') }
        id={ htmlId }
        onClick={ this.handleChange }
        onFocus={ this.handleOnFocus }
        onBlur={ this.handleOnBlur }
      >
        <StatusIcon width={ iconSize } height={ iconSize } />
      </div>
    );
  };

  getValidationMessage = () => {
    return null;
  };

  render() {
    const { className, withLabel, label, value } = this.props;
    const { htmlId } = this.state;
    const labelProps = {
      htmlId,
      value: `${ value }`,
      className: 'checkbox',
      getRef: this.getLabelRef,
    };
    const input = this.getInput();
    const validationMessage = this.getValidationMessage();
    return(
      <div className={ classNames(this.classCSS, isNotEmpty(className) && className) }>
        { withLabel && <Label { ...labelProps }>{ label }</Label> }
        { input }
        { validationMessage }
      </div>
    )
  }
}

export default Checkbox;
