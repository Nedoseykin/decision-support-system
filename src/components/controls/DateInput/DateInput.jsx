import './DateInput.scss';
import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Label from '../Label/Label';
import DateTable from './DateTable/DateTable';
import IconCalendar from '../../icons/IconCalendar';

import { isNotEmpty } from '../../../utils/utils';
import { COLORS } from '../../../consts/theme_consts';
import { CALENDAR_MODES } from './DateInput_const';
import MonthList from './MonthList/MonthList';
import YearList from './YearList/YearList';

const focusColor = COLORS.FOCUS_COLOR;
const blurColor = COLORS.BLUR_COLOR;

class DateInput extends PureComponent {
  static classId = 0;

  static getId = htmlId => isNotEmpty(htmlId)
    ? htmlId
    : `DateInput_${ DateInput.classId }`;

  static propTypes = {
    className         : PropTypes.string,
    withLabel         : PropTypes.bool,
    withValidation    : PropTypes.bool,
    label             : PropTypes.string,
    value             : PropTypes.number,
    type              : PropTypes.string,
    validationMessage : PropTypes.string,
    valueConverter    : PropTypes.func,
    onChange          : PropTypes.func,
  };

  static defaultProps = {
    className         : null,
    withLabel         : true,
    withValidation    : false,
    label             : null,
    value             : new Date().getTime(),
    type              : 'text',
    validationMessage : null,
    valueConverter    : null,
    onChange          : () => {},
  };

  constructor(props) {
    super(props);
    DateInput.classId++;
    this.classCSS = 'DateInput';
    this.state = {
      htmlId: DateInput.getId(props.htmlId),
      open: false,
      mode: CALENDAR_MODES.DATE_TABLE,
    };
    this.calendarBox = null;
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.handleDocumentMouseUp);
  }

  handleChange = (value, closeAfter) => () => {
    // console.log('value: ', value);
    const v = (typeof value === 'string')
      ? this.getNumberValue(value)
      : value;
    const { onChange } = this.props;
    !!onChange && onChange( v );
    if (closeAfter) this.closeCalendar();
  };

  handleChangeMode = mode => () => {
    this.setState({ mode });
  };

  handleOnFocus = () => {
    this.label.style.color = focusColor;
    this.input.style.borderBottomColor = focusColor;
  };

  handleOnBlur = () => {
    this.label.style.color = blurColor;
    this.input.style.borderBottomColor = blurColor;
  };

  handleCalendarToggle = event => {
    event.preventDefault();
    event.stopPropagation();
    if ( this.state.open ) {
      this.closeCalendar();
    } else {
      this.openCalendar();
    }
  };

  handleDocumentMouseUp = event => {
    let elm = event.target;
    // console.log('elm: ', elm);
    let test = false;
    if (!elm) return;
    const calendarBox = ReactDom.findDOMNode(this.calendarBox);
    while((elm !== document.body)) {
      if ( elm === calendarBox ) {
        test = true;
        break;
      }
      elm = elm.parentNode;
    }
    if (!test) this.closeCalendar();
  };

  closeCalendar = () => {
    document.removeEventListener('mouseup', this.handleDocumentMouseUp);
    this.setState({ open: false, mode: CALENDAR_MODES.DATE_TABLE });
  };

  openCalendar = () => {
    this.setState({ open: true, mode: CALENDAR_MODES.DATE_TABLE }, () => {
      document.addEventListener('mouseup', this.handleDocumentMouseUp, false);
    });
  };

  getNumberValue = value => {
    let result = new Date().getTime();
    try {
      result = new Date(value).getTime();
    } catch (err) {
      result = new Date().getTime();
    }
    return result;
  };

  getStringValue = value => {
    const { valueConverter } = this.props;
    let result = '';
    try {
      result = !!valueConverter
        ? valueConverter(value)
        : new Date(value).toLocaleString();
    } catch(err) {
      result = '';
    }
    return result;
  };

  getLabelRef = ref => { this.label = ref; };

  getInput = () => {
    const { type, value } = this.props;
    const strValue = this.getStringValue( value );
    const { htmlId } = this.state;
    return (
      <div
        key='inputBox'
        className={ classNames('date-input--input-box') }
      >
        <input
          id={ htmlId }
          type={ type }
          value={ strValue }
          ref={ ref => { this.input = ref; } }
          onChange={ this.handleChange }
          onFocus={ this.handleOnFocus }
          onBlur={ this.handleOnBlur }
          readOnly={ true }
        />
        <div
          className={ classNames('date-input--icon-box') }
          onMouseUp={ this.handleCalendarToggle }
        >
          <IconCalendar width={ 16 } height={ 16 } />
        </div>
      </div>
    );
  };

  getCalendar = (value, open, mode) => {
    let Content = null;
    // console.log('day: ', day);
    switch (mode) {
      case CALENDAR_MODES.DATE_TABLE : Content = DateTable; break;
      case CALENDAR_MODES.MONTH_LIST : Content = MonthList; break;
      case CALENDAR_MODES.YEAR_LIST  : Content = YearList; break;
      default:
    }
    return (
      <div
        key='calendar'
        className={ classNames('date-input--calendar-box', open ? 'calendar-opened' : 'calendar-closed') }
        ref={ elm => { this.calendarBox = elm; } }
      >
        {
          !!Content && (
            <Content
              value={value}
              open={ open }
              cbChanged={ this.handleChange }
              cbChangeMode={ this.handleChangeMode }
            />
          )
        }
      </div>
    );
  };

  getValidationMessage = () => {
    return null;
  };

  render() {
    const { className, withLabel, label, value } = this.props;
    const { open, mode } = this.state;
    const { htmlId } = this.state;
    const labelProps = {
      htmlId,
      value: this.getStringValue(value),
      className: 'date-input',
      getRef: this.getLabelRef,
    };
    const input = this.getInput();
    const calendar = this.getCalendar(value, open, mode);
    const validationMessage = this.getValidationMessage();
    return(
      <div className={ classNames( this.classCSS, isNotEmpty(className) && className ) }>
        { withLabel && <Label { ...labelProps }>{ label }</Label> }
        { input }
        { calendar }
        { validationMessage }
      </div>
    )
  }
}

export default DateInput;
