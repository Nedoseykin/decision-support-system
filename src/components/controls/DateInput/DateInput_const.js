export const MONTHS = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь'
];

export const DAYS_SHORT = [
  'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'
];

export const CALENDAR_MODES = {
  DATE_TABLE : 'DATE_TABLE',
  MONTH_LIST : 'MONTH_LIST',
  YEAR_LIST  : 'YEAR_LIST',
};
