export const setMonth = (month, value) => {
  const newDate = new Date(value);
  newDate.setMonth(month);
  const newMonth = newDate.getMonth();
  if (newMonth > month) {
    newDate.setDate(0);
  }
  return newDate.getTime();
};
