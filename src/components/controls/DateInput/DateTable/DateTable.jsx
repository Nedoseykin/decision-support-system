import React, { Fragment } from 'react';
import classNames from 'classnames';
import { CALENDAR_MODES, DAYS_SHORT, MONTHS } from '../DateInput_const';

const DateTable = ({ value, cbChanged, cbChangeMode }) => {
  const date = new Date(value);
  const year = date.getFullYear();
  const month = date.getMonth();
  const days = [];
  const first = new Date(value);
  first.setDate(1);
  const firstWD = first.getDay();
  for ( let i = 0; i < 6; i++ ) {
    days[ i ] = [];
    for ( let j = 0; j < 7; j++ ) {
      const d = new Date(first);
      d.setDate((j - firstWD + 2) + i * 7);
      /*console.log('d: ', d);*/
      days[ i ][ j ] = d;
    }
  }
  return (
    <Fragment>
      <div
        key='MMYY'
        className={ classNames('calendar--mmyy-box') }
      >
          <span
            key='month'
            className='mmyy-item'
            onClick={ cbChangeMode( CALENDAR_MODES.MONTH_LIST ) }
          >
            { MONTHS[ month ] }
          </span>
        { ', ' }
        <span
          key='year'
          className='mmyy-item'
          onClick={ cbChangeMode( CALENDAR_MODES.YEAR_LIST ) }
        >
            { year }
          </span>
      </div>
      <div
        key='DD'
        className={ classNames('calendar--dd') }
      >
        <div key='weekDays' className='calendar--week-days'>
          {
            DAYS_SHORT.map( (ds, i) => (
              <div key={ i } className='calendar--wd'>
                { ds }
              </div>
            ) )
          }
        </div>
        {
          days.map((week, i) => (
            <div key={ `week${ i }` } className='calendar--week'>
              {
                week.map( dd => (
                  <div
                    key={ `day${ dd.getDate() }` }
                    className={ classNames(
                      'calendar--day',
                      (month === dd.getMonth()) && 'current-month',
                      (value === dd.getTime()) && 'current-date'
                    ) }
                    onClick={ cbChanged(dd.getTime(), true) }
                  >
                    { dd.getDate() }
                  </div>
                ) )
              }
            </div>
          ))
        }
      </div>
    </Fragment>
  )
};

export default DateTable;
