import React from 'react';
import classNames from 'classnames';
import Scrollbars from '../../../CustomScrollbars/CustomScrollbars';
import { CALENDAR_MODES, MONTHS } from '../DateInput_const';
import { setMonth } from '../DateInput_utils';

const handleClick = (value, cbChanged, cbChangeMode) => () => {
  !!cbChanged && cbChanged(value)();
  !!cbChangeMode && cbChangeMode(CALENDAR_MODES.DATE_TABLE)();
};

const MonthList = ( { value, cbChanged, cbChangeMode } ) => {
  let date = new Date(value);
  const month = date.getMonth();
  const items = MONTHS.map((monthName, i) => {
    // date.setMonth(i);
    // let value = date.getTime();
    let newValue = setMonth(i, value);
    return (
      <div
        key={ i }
        className={ classNames('month-list--item', (i===month) && 'month-current') }
        onClick={ handleClick(newValue, cbChanged, cbChangeMode) }
      >
        { monthName }
      </div>
    )
  });
  return (
    <div className={ classNames('calendar--month-list') }>
      <Scrollbars>
        { items }
      </Scrollbars>
    </div>
  )
};

export default MonthList;
