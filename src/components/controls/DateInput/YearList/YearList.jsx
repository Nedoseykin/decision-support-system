import React, { useEffect } from 'react';
import classNames from 'classnames';
import Scrollbars from '../../../CustomScrollbars/CustomScrollbars';
import { CALENDAR_MODES } from '../DateInput_const';

const handleClick = (value, cbChanged, cbChangeMode) => () => {
  !!cbChanged && cbChanged(value)();
  !!cbChangeMode && cbChangeMode(CALENDAR_MODES.DATE_TABLE)();
};

const YEARS = [];
for ( let i = 1900; i < 2100; i++ ) {
  YEARS.push(i);
}

const YearList = ( { value, cbChanged, cbChangeMode } ) => {
  let box = null;
  const date = new Date(value);
  const year = date.getFullYear();
  const items = YEARS.map((yyyy, i) => {
    date.setFullYear(yyyy);
    let value = date.getTime();
    return (
      <div
        key={ i }
        className={ classNames('year-list--item', (yyyy===year) && 'year-current') }
        onClick={ handleClick(value, cbChanged, cbChangeMode) }
      >
        { yyyy }
      </div>
    )
  });
  useEffect(() => {
    const content = box.querySelector('.custom-scrollbars--content');
    if (!content) return null;
    const { offsetHeight: contentHeight, scrollTop } = content;
    const itemElm = content.querySelector('.year-current');
    if (!itemElm) return null;
    const { offsetTop: top, offsetHeight: height } = itemElm;
    if ((top + height - scrollTop) > contentHeight) {
      content.scrollTop = top - contentHeight / 2 + height / 2;
    }
  });
  return (
    <div
      className={ classNames('calendar--year-list') }
      ref={ elm => { box = elm; } }
    >
      <Scrollbars>
        { items }
      </Scrollbars>
    </div>
  )
};

export default YearList;
