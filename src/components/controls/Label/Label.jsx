import './Label.scss';
import React from 'react';
import classNames from 'classnames';
import { isNotEmpty } from '../../../utils/utils';

const Label = props => {
  const { children, value, htmlId, className, getRef } = props;
  return (
    <div
      className={ classNames(
        'Label',
        `${ className }--label-box`,
        isNotEmpty(value) && 'not-empty'
      ) }
    >
      <label
        htmlFor={ htmlId }
        ref={ ref => { getRef( ref ) } }
      >
        { children }
      </label>
    </div>
  );
};

export default Label;
