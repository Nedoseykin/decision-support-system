import './RadioInput.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isNotEmpty } from '../../../utils/utils';
import Label from '../Label/Label';
import IconUnselected from '../../icons/IconRadioUnselected';
import IconSelected from '../../icons/IconRadioSelected';
// import { SEX } from '../../../consts/common_consts';

class RadioInput extends PureComponent {
  static classId = 0;

  static getId = htmlId => isNotEmpty(htmlId)
    ? htmlId
    : `RadioInput_${ RadioInput.classId }`;

  static propTypes = {
    className         : PropTypes.string,
    withLabel         : PropTypes.bool,
    withValidation    : PropTypes.bool,
    label             : PropTypes.string,
    value             : PropTypes.any.isRequired,
    itemsList         : PropTypes.arrayOf( PropTypes.object ),
    // type              : PropTypes.string,
    validationMessage : PropTypes.string,
    valueConverter    : PropTypes.func,
    onChange          : PropTypes.func,
  };

  static defaultProps = {
    className         : null,
    withLabel         : true,
    withValidation    : false,
    label             : null,
    // value             : SEX.MAIL,
    itemsList         : [],
    // type              : 'text',
    validationMessage : null,
    valueConverter    : null,
    onChange          : () => {},
  };

  constructor(props) {
    super(props);
    RadioInput.classId++;
    this.classCSS = 'RadioInput';
    this.state = {
      htmlId: RadioInput.getId(props.htmlId),
    };
  }

  handleChange = id => () => {
    const { onChange } = this.props;
    !!onChange && onChange(id);
  };

  getLabelRef = ref => { this.label = ref; };

  getInput = () => {
    const { itemsList, value } = this.props;
    const items = itemsList.map(item => {
      const { id, label } = item;
      const IconItem = value === id
        ? IconSelected
        : IconUnselected;
      return (
      <div
        key={ id }
        className={ classNames('radio-item', (id === value) && 'radio-item--current') }
      >
        <div
          key='iconBox'
          className={ classNames('radio-item--icon-box') }
          onClick={ this.handleChange(id) }
        >
          <IconItem width={ 16 } height={ 16 } />
        </div>
        <div
          key='labelBox'
          className={ classNames('radio-item--label-box') }
          onClick={this.handleChange(id)}
        >
          { label }
        </div>
      </div>
    )});
    return (
      <div key='input' className='radio-input--input-box'>
        { items }
      </div>
    );
  };

  getValidationMessage = () => {
    return null;
  };

  render() {
    const {
      className,
      withLabel,
      label,
      value,
    } = this.props;
    const { htmlId } = this.state;
    const labelProps = {
      htmlId,
      value,
      className: 'radio-input',
      getRef: this.getLabelRef,
    };
    const input = this.getInput();
    const validationMessage = this.getValidationMessage();
    return(
      <div className={ classNames( this.classCSS, isNotEmpty(className) && className ) }>
        { withLabel && <Label { ...labelProps }>{ label }</Label> }
        { input }
        { validationMessage }
      </div>
    )
  }
}

export default RadioInput;
