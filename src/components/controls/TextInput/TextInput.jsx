import './TextInput.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Label from '../Label/Label';

import { isNotEmpty } from '../../../utils/utils';
import { COLORS } from '../../../consts/theme_consts';

const focusColor = COLORS.FOCUS_COLOR;
const blurColor = COLORS.BLUR_COLOR;

class TextInput extends PureComponent {
  static classId = 0;

  static getId = htmlId => isNotEmpty(htmlId)
    ? htmlId
    : `TextInput_${ TextInput.classId }`;

  static propTypes = {
    className: PropTypes.string,
    withLabel: PropTypes.bool,
    withValidation: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.string,
    type:  PropTypes.string,
    validationMessage: PropTypes.string,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    className: null,
    withLabel: true,
    withValidation: false,
    label: null,
    value: null,
    type: 'text',
    validationMessage: null,
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    TextInput.classId++;
    this.classCSS = 'TextInput';
    this.state = {
      htmlId: TextInput.getId(props.htmlId),
    };
  }

  handleChange = event => {
    const { onChange } = this.props;
    const { value } = event.target;
    !!onChange && onChange( value );
  };

  handleOnFocus = () => {
    const { withLabel } = this.props;
    withLabel && (this.label.style.color = focusColor);
    this.input.style.borderBottomColor = focusColor;
  };

  handleOnBlur = () => {
    const { withLabel } = this.props;
    withLabel && (this.label.style.color = blurColor);
    this.input.style.borderBottomColor = blurColor;
  };

  getLabelRef = ref => { this.label = ref; };

  getInput = () => {
    const { type, value } = this.props;
    const { htmlId } = this.state;
    return (
      <div
        key='inputBox'
        className={ classNames('text-input--input-box') }
      >
        <input
          id={ htmlId }
          type={ type }
          value={ value }
          ref={ ref => { this.input = ref; } }
          onChange={ this.handleChange }
          onFocus={ this.handleOnFocus }
          onBlur={ this.handleOnBlur }
        />
      </div>
    );
  };

  getValidationMessage = () => {
    return null;
  };

  render() {
    const { className, withLabel, label, value } = this.props;
    const { htmlId } = this.state;
    const labelProps = {
      htmlId,
      value,
      className: 'text-input',
      getRef: this.getLabelRef,
    };
    const input = this.getInput();
    const validationMessage = this.getValidationMessage();
    return(
      <div className={ classNames(this.classCSS, isNotEmpty(className) && className) }>
        { withLabel && <Label { ...labelProps }>{ label }</Label> }
        { input }
        { validationMessage }
      </div>
    )
  }
}

export default TextInput;
