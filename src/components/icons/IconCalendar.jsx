import React from 'react';

const IconCalendar = ({ width, height }) => (
  <svg
    xmlns ="http://www.w3.org/2000/svg"
    width={ width }
    height={ height }
    viewBox='0 0 16 16'
  >
    <g>
      <path
        d='M3 1 V0 H5 V1 M11 1 V0 H13 V1'
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
      <path
        d='M1 2 H15 V15 H1 z'
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
      <path
        d='M4.5 3 V14 M8 3 V14 M11.5 3 V14'
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
      <path
        d='M2 5.5 H14 M2 8.5 H14 M2 11.5 H14'
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
    </g>
  </svg>
);

export default IconCalendar;
