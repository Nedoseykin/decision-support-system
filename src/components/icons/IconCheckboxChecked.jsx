import React from 'react';

const IconCheckboxChecked = ({ width, height }) => (
  <svg
    xmlns ="http://www.w3.org/2000/svg"
    width={ width }
    height={ height }
    viewBox='0 0 16 16'
  >
    <g>
      <rect
        x='1'
        y='1'
        width='14'
        height='14'
        rx='3'
        ry='3'
        stroke='rgba(0,0,0,1)'
        strokeWidth='1'
        fill='rgba(0,0,0,1)'
      />
      <path
        d='M4 6 L7 12 L12 3'
        stroke='rgba(255,255,255,1)'
        strokeWidth='1.5'
        fill='none'
      />
    </g>
  </svg>
);

export default IconCheckboxChecked;
