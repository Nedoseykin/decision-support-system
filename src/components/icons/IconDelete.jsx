import React from 'react';
import { THEME } from '../../consts/style_const/theme';

const defaultStroke = THEME.colors.contrastBlack;

const IconDelete = ({ width, height, stroke = defaultStroke }) => (
  <svg
    xmlns ="http://www.w3.org/2000/svg"
    width={ width }
    height={ height }
    viewBox='0 0 16 16'
  >
    <g>
      <path
        d='M4 8 H12'
        stroke={ stroke }
        strokeWidth='1.5'
        fill='none'
      />
    </g>
  </svg>
);

export default IconDelete;
