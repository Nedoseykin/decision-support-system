import React from 'react';

const IconRadioSelected = ({ width, height }) => (
  <svg
    xmlns ="http://www.w3.org/2000/svg"
    width={ width }
    height={ height }
    viewBox='0 0 16 16'
  >
    <g>
      <circle
        cx={ 8 }
        cy={ 8 }
        r={ 7 }
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
      <circle
        cx={ 8 }
        cy={ 8 }
        r={ 5 }
        stroke='rgba(0,0,0,1)'
        fill='rgba(0,0,0,1)'
      />
    </g>
  </svg>
);

export default IconRadioSelected;
