import React from 'react';

const IconRadioUnselected = ({ width, height }) => (
  <svg
    xmlns ="http://www.w3.org/2000/svg"
    width={ width }
    height={ height }
    viewBox='0 0 16 16'
  >
    <g>
      <circle
        cx={ 8 }
        cy={ 8 }
        r={ 7 }
        stroke='rgba(0,0,0,1)'
        fill='none'
      />
    </g>
  </svg>
);

export default IconRadioUnselected;
