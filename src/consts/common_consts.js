const SEX = {
  MAIL: 'MAIL',
  FEMAIL: 'FEMAIL'
};

const ETHIOLOGY_ID = {
  VENOUS         : 'VENOUS',
  ARTERIAL       : 'ARTERIAL',
  DIABETIC       : 'DIABETIC',
  POSTTHRAUMATIC : 'POSTTHRAUMATIC',
  BED_SORE       : 'BED_SORE',
};

const ETHIOLOGY_ITEMS = [
  {
    id: ETHIOLOGY_ID.VENOUS,
    label: 'Венозная',
    bit: 1,
  },
  {
    id: ETHIOLOGY_ID.ARTERIAL,
    label: 'Артериальная',
    bit: 2,
  },
  {
    id: ETHIOLOGY_ID.DIABETIC,
    label: 'Диабетическая',
    bit: 4,
  },
  {
    id: ETHIOLOGY_ID.POSTTHRAUMATIC,
    label: 'Посттравматическая',
    bit: 8,
  },
  {
    id: ETHIOLOGY_ID.BED_SORE,
    label: 'Пролежневая',
    bit: 16,
  },
];

export {
  SEX,
  ETHIOLOGY_ID,
  ETHIOLOGY_ITEMS,
};
