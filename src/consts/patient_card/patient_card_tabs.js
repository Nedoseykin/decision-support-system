import { PATIENT_CARD_TAB_ID } from './patient_card_tab_id';
import { SEX } from '../common_consts';
import PassportData from '../../components/PageBuilder/pages/PatientPage/MainSection/PatientCard/PassportData';
import UlcerCharacteristics
  from '../../components/PageBuilder/pages/PatientPage/MainSection/PatientCard/UlcerCharacteristics';
import Visits from '../../components/PageBuilder/pages/PatientPage/MainSection/PatientCard/Visits';
import Procedures from '../../components/PageBuilder/pages/PatientPage/MainSection/PatientCard/Procedures';

const {
  PASSPORT_DATA,
  ULCER_CHARACTERISTICS,
  VISITS,
  PROCEDURES,
} = PATIENT_CARD_TAB_ID;

const PATIENT_CARD_TABS = [
  {
    id        : PASSPORT_DATA,
    label     : 'Паспортные данные',
    component : PassportData,
    state     : {
      firstName           : '',
      middleName          : '',
      lastName            : '',
      address             : '',
      mainDiagnosis       : '',
      concurrentDiagnosis : '',
      creationDate        : new Date().getTime(),
      birthDate           : new Date().getTime(),
      sex                 : SEX.MAIL,
      age                 : 0,
      phone               : '',
    },
  },
  {
    id        : ULCER_CHARACTERISTICS,
    label     : 'Характеристика язвы',
    component : UlcerCharacteristics,
    state     : {
      ulcerEthiology         : 0, // bits operations
      ulcerAnamnesisDuration : 0, // months
      lastPersistDuration    : 0, // months
      previousOperations     : '',
      previousTreatment      : '',
      ulcerLocalization      : '',
    },
  },
  {
    id        : VISITS,
    label     : 'Посещения',
    component : Visits,
    state     : {
      columns: [
        {
          id: 'id',
          value: 'ID',
          columnName: 'ID',
          visible: false,
          fixed: true,
          resizable: true,
          movable: false,
          width: 50,
        },
        {
          id: 'name',
          value: 'Визит',
          columnName: 'Визит',
          visible: true,
          fixed: true,
          resizable: true,
          movable: false,
          width: 200,
        },
        {
          id: 'size',
          value: 'Размеры',
          columnName: 'Размеры',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 150,
        },
        {
          id: 'square',
          value: 'Площадь',
          columnName: 'Площадь',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 150,
        },
        {
          id: 'delta',
          value: '+/- (%)',
          columnName: '+/- (%)',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 100,
        },
        {
          id: 'buttons',
          value: '',
          columnName: '',
          visible: true,
          fixed: false,
          resizable: false,
          movable: false,
          width: 88,
          tdIdKey: 'id',
        }
      ],
      rows: [],
    },
  },
  {
    id        : PROCEDURES,
    label     : 'Процедуры',
    component : Procedures,
    state     : {
      columns: [
        {
          id: 'id',
          value: 'ID',
          columnName: 'ID',
          visible: false,
          fixed: true,
          resizable: false,
          movable: false,
          width: 50,
        },
        {
          id: 'visitDate',
          value: 'Дата посещения',
          columnName: 'Дата посещения',
          visible: true,
          fixed: true,
          resizable: true,
          movable: false,
          width: 200,
        },
        {
          id: 'ablation',
          value: 'Абляция',
          columnName: 'Абляция',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 200,
        },
        {
          id: 'sma',
          value: 'SMA',
          columnName: 'SMA',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 200,
        },
        {
          id: 'delta',
          value: '+/- (%)',
          columnName: '+/- (%)',
          visible: true,
          fixed: false,
          resizable: true,
          movable: false,
          width: 100,
        },
        {
          id: 'buttons',
          value: '',
          columnName: '',
          visible: true,
          fixed: false,
          resizable: false,
          movable: false,
          width: 88,
          tdIdKey: 'id',
        }
      ],
      rows: [],
    },
  },
];

export {
  PATIENT_CARD_TABS,
};
