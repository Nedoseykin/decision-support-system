import { COLORS } from './theme_colors';

const THEME = {
  colors: COLORS,
};

export {
  THEME,
};
