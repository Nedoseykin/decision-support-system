const COLORS = {
  primary25     : 'rgba(183,220,255,1)',
  primary50     : 'rgba(71,166,255,1)',
  primary100    : 'rgba(30,144,255,1)',
  primary200    : 'rgba(28,134,238,1)',
  primary300    : 'rgba(24,116,205,1)',
  primary400    : 'rgba(16,78,139,1)',

  default50     : 'rgba(230,230,230,1)',
  default75     : 'rgba(193,193,193,1)',
  default100    : 'rgba(153,153,153,1)',
  default200    : 'rgba(112,112,112,1)',
  default300    : 'rgba(71,71,71,1)',
  default400    : 'rgba(31,31,31,1)',

  secondary25   : 'rgba(255,200,200,1)',
  secondary50   : 'rgba(255,169,169,1)',
  secondary100  : 'rgba(255,128,128,1)',
  secondary200  : 'rgba(255,84,84,1)',
  secondary300  : 'rgba(255,54,54,1)',
  secondary400  : 'rgba(255,0,0,1)',

  contrastWhite : 'rgba(255,255,255,1)',
  contrastBlack : 'rgba(0,0,0,1)',

  transparent   : 'rgba(255,255,255,0)',
};

export {
  COLORS,
};
