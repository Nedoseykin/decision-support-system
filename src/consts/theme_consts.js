import { THEME } from './style_const/theme';

export const COLORS = {
  FOCUS_COLOR                 : THEME.colors.primary300,    // 'rgba(24,116,205,1)',
  BLUR_COLOR                  : THEME.colors.default300,    // 'rgba(71,71,71,1)',
  TAB_BACKGROUND_COLOR        : THEME.colors.default75,     // 'rgba(193,193,193,1)',
  ACTIVE_TAB_BACKGROUND_COLOR : THEME.colors.contrastWhite, // 'rgba(255,255,255,1)',
};
