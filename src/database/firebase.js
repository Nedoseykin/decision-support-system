import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const config = {
  apiKey            : "AIzaSyA5Y-P_iU__9onzS2SgU0np7Eo3vbVwX4A",
  authDomain        : "decision-support-system-5d215.firebaseapp.com",
  databaseURL       : "https://decision-support-system-5d215.firebaseio.com",
  projectId         : "decision-support-system-5d215",
  storageBucket     : "decision-support-system-5d215.appspot.com",
  messagingSenderId : "996623077189"
};

firebase.initializeApp( config );

export default firebase;

export const database = firebase.database();
