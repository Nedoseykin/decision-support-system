import { database } from './firebase';

const getData = callback => {
  let ref = database.ref('/');
  ref.on('value', snapshot => {
    const data = snapshot.val();
    !!callback && callback(data);
    console.log('DATA RETRIEVED: ', data);
  })
};

const writeData = data => {
  database.ref('/').set(data);
  console.log('DATA SAVED: ', data);
};

const orderByAge = callback => {
  let ref = database.ref('/');
  ref.orderByChild('age').on('child_added', snapshot => {
    console.log(`orderByValue: name: ${ snapshot.val().name }, age: ${ snapshot.val().age }`);
    !!callback && callback(snapshot.val());
  })
};

export {
  getData,
  writeData,
  orderByAge,
};