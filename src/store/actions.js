import { createAction } from 'redux-actions';

export const acLogin = createAction('AC_LOGIN');

export const acLogout = createAction('AC_LOGOUT');

export const acSetValue = createAction('AC_SET_VALUE');

export const acUiUpdateTableData = createAction('AC_UI_UPDATE_TABLE_DATA');

// patient card
export const acUISetActiveTab = createAction('AC_UI_SET_TAB_VALUE');

export const acUiSetPatientCardTabs = createAction('AC_UI_SET_PATIENT_CARD_TABS');

export const acResetApp = createAction('AC_RESET_APP');
