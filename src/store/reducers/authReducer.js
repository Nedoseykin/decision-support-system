import { handleActions } from 'redux-actions';

import { acLogin, acLogout, acResetApp } from '../actions';

const initialState = {
  isLogged: true,
};

export default handleActions({
  [ acLogin ]: ( state ) => ({
    ...state,
    isLogged: true,
  }),
  [ acLogout ]: state => ({
    ...state,
    isLogged: false,
  }),
  [ acResetApp ]: () => ({
    ...initialState,
  }),
}, initialState);
