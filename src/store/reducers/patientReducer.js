import { handleActions } from 'redux-actions';

import {
  acSetValue,
  acResetApp,
} from '../actions';

import { SEX } from '../../consts/common_consts';

const initialState = {
  firstName: null,
  thirdName: null,
  familyName: null,
  birthDate: new Date().getTime(),
  sex: SEX.MAIL,
  hospitalized: false,
};

export default handleActions({
  [ acSetValue ]: (state, { payload }) => ({
    ...state,
    [ payload.name ]: payload.value,
  }),

  [ acResetApp ]: () => ({
    ...initialState,
  }),
}, initialState);
