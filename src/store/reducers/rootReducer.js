import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import auth from './authReducer';
import patient from './patientReducer';
import ui from './uiReducer';

export default history => combineReducers({
  router: connectRouter(history),
  auth,
  patient,
  ui,
})