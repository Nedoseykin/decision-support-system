import { handleActions } from 'redux-actions';
import { customTableColumns, customTableRows } from '../../mocks/customTable/customTableData';
import { calculateRows } from '../../utils/ui/table_ui_utils';

import {
  acUiUpdateTableData,
  acUISetActiveTab,
  acUiSetPatientCardTabs,
  acResetApp,
} from '../actions';
import { PATIENT_CARD_TAB_ID } from '../../consts/patient_card/patient_card_tab_id';
import { PATIENT_CARD_TABS } from '../../consts/patient_card/patient_card_tabs';

const { PASSPORT_DATA, } = PATIENT_CARD_TAB_ID;

const initialState = {
  patientCardActiveTabId : PASSPORT_DATA,
  patientCardTabs        : [ ...PATIENT_CARD_TABS ],

  // for test
  table: {
    columns: customTableColumns,
    rows: calculateRows(customTableRows, customTableColumns),
  }
};

export default handleActions({
  [ acUISetActiveTab ]: ( state, { payload }) => {
    return {
      ...state,
      patientCardActiveTabId: payload.activeTabId,
    }
  },
  [ acUiSetPatientCardTabs ]: (state, { payload }) => {
    return {
      ...state,
      patientCardTabs: payload.tabs
    }
  },
  [ acUiUpdateTableData ]: ( state, { payload }) => {
    // payload: { columns, rows }
    return {
      ...state,
      table: {
        ...state.table,
        ...payload,
      },
    }
  },

  [ acResetApp ]: () => {
    return {
      ...initialState,
    }
  },
}, initialState);