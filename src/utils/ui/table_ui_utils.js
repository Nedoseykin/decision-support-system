import { store } from '../../store/configureStore';
import { findArrayItem, isInteger, isNotEmpty, isNotEmptyArray } from '../utils';
import { acUiSetPatientCardTabs } from '../../store/actions';

const calculateRows = (rowsData, columnsData) => {
  let rows = isNotEmptyArray( rowsData ) && isNotEmptyArray( columnsData )
    ? rowsData.map(row => {
      const { id: rowId } = row;
      return {
        id: rowId,
        cells: columnsData.map(col => {
          const {
            id: colId,
            tdValueMaker,
            tdIdKey,
          } = col;
          const idKey = isNotEmpty( tdIdKey ) ? tdIdKey : colId;
          const value = !!tdValueMaker ? tdValueMaker(row[ idKey ]) : row[ idKey ];
          return {
            id: colId,
            value,
          }
        })
      }
    })
    : [];

  return rows;
};

// @param value - <Number>, integer
// @parem list - array <String> ['год', 'года', 'лет' ]
const getDecimalStrValue = (v, list) => {
  // console.log('getDecimalStrValue: v: ', v);
  if (!isInteger(v) || !isNotEmptyArray(list) || list.length !== 3) return '';
  const result = i => `${ v } ${ list[ i ] }`;
  let s = `${ v }`.substr(-2,2);
  let ind = 2;
  if (s.length < 2 || s[ 0 ] !== '1') {
    s = s.substr(-1,1);
    const iVal = parseInt(s);
    if (iVal === 1) ind = 0;
    else if (iVal > 1 && iVal < 5) ind = 1;
  }
  console.log('getDecimalStrValue: result: ', result(ind));
  return result(ind);
};

const getMonthCountToMYStr = v => {
  // console.log('getMonthCountToMYStr: v: ', v);
  if (v === 0) return 'Первое посещение';
  let result = '';
  if (isInteger(v)) {
    console.log('getMonthCountToMYStr: v in IF: ', v);
    let years = Math.floor(v / 12);
    let months = v % 12;
    // console.log('getMonthCountToMYStr: years: ', years);
    // console.log('getMonthCountToMYStr: months: ', months);
    if (years > 0) result = getDecimalStrValue(years, [ 'год', 'года', 'лет' ]);
    if (months > 0) result = `${ result } ${ getDecimalStrValue(months, [ 'месяц', 'месяца', 'месяцев' ]) }`;
  }
  // console.log('getMonthCountToMYStr: result: ', result);
  return result;
};

const getTabState = tabId => {
  const { patientCardTabs } = store.getState().ui;
  const tab = isNotEmptyArray(patientCardTabs)
    ? findArrayItem(patientCardTabs, { id: tabId })
    : null;
  return !!tab ? tab.state : null;
};

const updateTabState = (tabId, data) => {
  let { patientCardTabs } = store.getState().ui;
  patientCardTabs = patientCardTabs.map(tab => {
    const { id, state, ...rest } = tab;
    return {
      id,
      state: id === tabId ? { ...state, ...data } : state,
      ...rest,
    }
  });
  store.dispatch(acUiSetPatientCardTabs({ tabs: patientCardTabs }));
};

export {
  calculateRows,
  getDecimalStrValue,
  getMonthCountToMYStr,
  getTabState,
  updateTabState,
}
