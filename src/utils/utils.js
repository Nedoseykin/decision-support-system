const isExists = v => v !== undefined && v !== null;

const isNotEmpty = v => isExists(v) && v.length > 0;

const isNotEmptyArray = v => Array.isArray(v) && v.length > 0;

const isInteger = v => {
  const j = parseFloat(v);
  const i = parseInt(v);
  return (!isNaN(i) && isFinite(i) && (j === i) && (typeof v === 'number'));
};

const addZero = (v, n) => {
  let result = `${ v }`;
  const count = n - result.length;
  if ( count > 0 ) {
    for ( let i = 0; i < count; i++ ) result = `0${ result }`;
  }
  return result;
};

// v = Date().getTime() - timestamp
const dateToDDMMYYY = (v, d = '.') => {
  let result = '';
  try {
    const value = new Date(v);
    const date = addZero(value.getDate(), 2);
    const month = addZero(value.getMonth() + 1,2);
    const year = value.getFullYear();
    result = [date, month, year].join(d);
  } catch(err) {}
  return result;
};

const findArrayItemIndex = (arr, flt, isStrict = true) => {
  const fltKeys = Object.keys(flt);
  if (!isNotEmptyArray(arr) || !flt) return -1;
  for (let i = 0; i < arr.length; i++) {
    let test = true;
    for (let j = 0; j < fltKeys.length; j++) {
      const condition = isStrict
        ? arr[ i ][ fltKeys[ j ] ] === flt[ fltKeys[ j ] ]
        : arr[ i ][ fltKeys[ j ] ] == flt[ fltKeys[ j ] ];
      if (!condition) {
        test = false;
        break;
      }
    }
    if (test) return i;
  }
  return -1;
};

const findArrayItem = (arr, flt, isStrict = true) => {
  const i = findArrayItemIndex(arr, flt, isStrict);
  return i > -1 ? arr[ i ] : null;
};

export {
  addZero,
  dateToDDMMYYY,
  isExists,
  isNotEmpty,
  isNotEmptyArray,
  isInteger,
  findArrayItemIndex,
  findArrayItem,
};
