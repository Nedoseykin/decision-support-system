import './CustomScrollbars.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isNotEmpty } from '../../utils/utils';

const VERTICAL = 'VERTICAL';
const HORIZONTAL = 'HORIZONTAL';

class CustomScrollbars extends PureComponent {
  static classId = 0;

  static getHtmlId = id => {
    if (id) return id;
    return `CustomScrollbars-${ CustomScrollbars.classId }`;
  };

  static propTypes = {
    className               : PropTypes.string,
    isHorizontal            : PropTypes.bool,
    isVertical              : PropTypes.bool,
    disableHorizontalScroll : PropTypes.bool,
    width                   : PropTypes.oneOfType([
      PropTypes.string, PropTypes.number,
    ]),
    height                  : PropTypes.oneOfType([
      PropTypes.string, PropTypes.number,
    ]),
    hideScrollbars          : PropTypes.bool,
    getRef                  : PropTypes.func,
    getScroller             : PropTypes.func,
    propagation             : PropTypes.bool,
    onChange                : PropTypes.func,
    // (scrollLeft, scrollTop, scrollWidth, scrollHeight, offsetWidth, offsetHeight) => { ... }
  };

  static defaultProps = {
    className               : null,
    isHorizontal            : true,
    isVertical              : true,
    disableHorizontalScroll : false,
    width                   : '100%',
    height                  : '100%',
    hideScrollbars          : false,
    getRef                  : null,
    getScroller             : null,
    propagation             : false,
    onChange                : null,
  };

  constructor(props) {
    super(props);
    CustomScrollbars.classId++;
    this.state = {
      htmlId: CustomScrollbars.getHtmlId(props.id),
    };
  }

  x = 0;
  y = 0;
  mode = null;
  component = null;
  content   = null;
  hHnd      = null;
  hSB       = null;
  hSBBar    = null;
  vHnd      = null;
  vSB       = null;
  vSBBar    = null;

  componentDidMount() {
    this.component = this.getByPostfixId();
    this.content   = this.getByPostfixId('content');
    this.hHnd      = this.getByPostfixId('hHnd');
    this.hSB       = this.getByPostfixId('hSB');
    this.hSBBar    = this.getByPostfixId('hSBBar');
    this.vHnd      = this.getByPostfixId('vHnd');
    this.vSB       = this.getByPostfixId('vSB');
    this.vSBBar    = this.getByPostfixId('vSBBar');

    this.setListeners();
    this.prepareStyle();
    const { getScroller } = this.props;
    !!getScroller && getScroller(this.scroll);
  }

  componentDidUpdate( prevProps, prevState, snapshot ) {
    this.prepareStyle();
  }

  componentWillUnmount() {
    this.removeListeners();
  }

  setListeners = () => {
    if (!!this.hHnd) this.hHnd.addEventListener('mousedown', this.handleHHndMouseDown, false);
    if (!!this.vHnd) this.vHnd.addEventListener('mousedown', this.handleVHndMouseDown, false);
    window.addEventListener('resize', this.handleResize, false);
  };

  removeListeners = () => {
    if (!!this.hHnd) this.hHnd.removeEventListener('mousedown', this.handleHHndMouseDown);
    if (!!this.vHnd) this.vHnd.removeEventListener('mousedown', this.handleVHndMouseDown);
    window.removeEventListener('resize', this.handleResize);
  };

  prepareStyle = () => {
    if (!!this.content) {
      const {
        scrollLeft: cntScLeft,
        scrollTop: cntScTop,
        scrollHeight: cntScHeight,
        scrollWidth: cntScWidth,
        offsetWidth: cntWidth,
        offsetHeight: cntHeight,
      } = this.content;

      const kHeight = cntHeight / cntScHeight;
      const kWidth = cntWidth / cntScWidth;

      const kLeft = cntScWidth > cntWidth
        ? cntScLeft / (cntScWidth - cntWidth)
        : cntScLeft / cntScWidth;

      const kTop = cntScHeight > cntHeight
        ? cntScTop / (cntScHeight - cntHeight)
        : cntScTop / cntScHeight;

      if (!!this.hSB && !!this.hHnd) {
        let width = this.hSB.offsetWidth * kWidth;
        (width < 32) && (width = 32);
        this.hHnd.style.width = `${ width }px`;
        this.hHnd.style.left = `${ (this.hSB.offsetWidth - width) * kLeft }px`;
        if (cntWidth >= cntScWidth) {
          this.hSB.style.visibility = 'hidden';
          !!this.vSB && (this.vSB.style.height = 'calc(100% - 8px)');
        } else {
          this.hSB.style.visibility = 'visible';
          !!this.vSB && (this.vSB.style.height = 'calc(100% - 20px)');
        }
      }

      if (!!this.vSB && !!this.vHnd) {
        let height = this.vSB.offsetHeight * kHeight;
        (height < 32) && (height = 32);
        this.vHnd.style.height = `${ height }px`;
        this.vHnd.style.top = `${ (this.vSB.offsetHeight - height) * kTop }px`;
        if (cntHeight >= cntScHeight) {
          this.vSB.style.visibility = 'hidden';
          !!this.hSB && (this.hSB.style.width = 'calc(100% - 8px)');
        } else {
          this.vSB.style.visibility = 'visible';
          !!this.hSB && (this.hSB.style.width = 'calc(100% - 20px)');
        }
      }

      const { onChange } = this.props;
      !!onChange && onChange(cntScLeft, cntScTop, cntScWidth, cntScHeight, cntWidth, cntHeight);
    }
  };

  handleResize = () => {
    this.prepareStyle();
  };

  handleHHndMouseDown = event => {
    event = event || window.event;
    event.stopPropagation();
    event.preventDefault();
    const { clientX } = event;
    this.x = clientX;
    this.mode = HORIZONTAL;
    !!this.hSBBar && ( this.hSBBar.dataset.active = `${ true }`);
    !!this.component && ( this.component.dataset.active = `${ true }` );
    document.addEventListener('mousemove', this.handleDocumentMouseMove, false);
    document.addEventListener('mouseup', this.handleDocumentMouseUp, false);
  };

  handleVHndMouseDown = event => {
    event = event || window.event;
    event.stopPropagation();
    event.preventDefault();
    const { clientY } = event;
    this.y = clientY;
    this.mode = VERTICAL;
    !!this.vSBBar && ( this.vSBBar.dataset.active = `${ true }`);
    !!this.component && ( this.component.dataset.active = `${ true }` );
    document.addEventListener('mousemove', this.handleDocumentMouseMove, false);
    document.addEventListener('mouseup', this.handleDocumentMouseUp, false);
  };

  handleWheel = event => {
    // !propagation && event.stopPropagation();
    const { deltaX, deltaY, currentTarget } = event;
    let sign = deltaX / Math.abs( deltaX );
    const {
      // scrollLeft,
      scrollTop,
      scrollHeight,
      offsetHeight,
    } = currentTarget;
    !this.props.disableHorizontalScroll && (currentTarget.scrollLeft += sign * 100);
    sign = deltaY / Math.abs( deltaY );
    if (deltaY < 0 && scrollTop > 0) event.stopPropagation();
    if (deltaY > 0 && (scrollHeight - scrollTop - offsetHeight) > 0) event.stopPropagation();
    currentTarget.scrollTop += sign * 100;
    this.prepareStyle();
  };

  handleDocumentMouseMove = event => {
    event.stopPropagation();
    event.preventDefault();
    // const content = this.getByPostfixId('content');

    if (!this.content) return null;
    // if (!content) return null;
    const { clientX, clientY } = event;
    const {
      offsetWidth,
      offsetHeight,
      scrollWidth,
      scrollHeight,
    } = this.content;

    if (this.mode === HORIZONTAL) {
      if (!this.hSB || !this.hHnd) return null;
      const { offsetWidth: width } = this.hSB;
      const sLength = width - this.hHnd.offsetWidth;
      const cLength = scrollWidth - offsetWidth;
      let k = 1;
      if (cLength > 0) k = cLength / sLength;
      const length = clientX - this.x;
      this.x = clientX;
      this.content.scrollLeft += length * k;
    }
    else if (this.mode === VERTICAL) {
      if (!this.vSB || !this.vHnd) return null;
      const { offsetHeight: height } = this.vSB;
      const sLength = height - this.vHnd.offsetHeight;
      const cLength = scrollHeight - offsetHeight;
      let k = 1;
      if (cLength > 0) {
        k = cLength / sLength;
      }
      const length = clientY - this.y;
      this.y = clientY;
      this.content.scrollTop += length * k;
    }

    this.prepareStyle();
  };

  handleDocumentMouseUp = event => {
    event = event || window.event;
    event.preventDefault();
    event.stopPropagation();
    this.mode = null;
    document.removeEventListener('mousemove', this.handleDocumentMouseMove);
    document.removeEventListener('mouseup', this.handleDocumentMouseUp);
    !!this.hSBBar && (this.hSBBar.dataset.active = `${ false }`);
    !!this.vSBBar && (this.vSBBar.dataset.active = `${ false }`);
    !!this.component && (this.component.dataset.active = `${ false }`);
  };

  handleSBClick = sb => event => {
    event.stopPropagation();
    event.preventDefault();
    const { clientX, clientY } = event;
    if (!this.content) return null;
    const {
      offsetWidth,
      offsetHeight,
      scrollWidth,
      scrollHeight,
    } = this.content;

    if (sb === HORIZONTAL) {
      if (!this.hSB || !this.hHnd) return null;
      let cLength = 0;
      if (scrollWidth > offsetWidth) cLength = scrollWidth - offsetWidth;
      const { width, left } = this.hSB.getBoundingClientRect();
      let sLength = 0;
      if (width > this.hHnd.offsetWidth) sLength = width - this.hHnd.offsetWidth;
      const kx = (cLength > 0) ? sLength / cLength : 1;
      const x = (clientX - left - this.hHnd.offsetWidth / 2) / kx;
      this.content.scrollLeft = x;
    } else if (sb === VERTICAL) {
      if (!this.vSB || !this.vHnd) return null;
      let cLength = 0;
      if (scrollHeight > offsetHeight) cLength = scrollHeight - offsetHeight;
      const { height, top } = this.vSB.getBoundingClientRect();
      let sLength = 0;
      if (height > this.vHnd.offsetHeight) sLength = height - this.vHnd.offsetHeight;
      const ky = (cLength > 0) ? sLength / cLength : 1;
      const y = (clientY - top - this.vHnd.offsetHeight / 2) / ky;
      this.content.scrollTop = y;
    }
    this.prepareStyle();
  };

  scroll = (scLeft, scTop) => {
    if ( !this.content ) return;
    this.content.scrollLeft = scLeft;
    this.content.scrollTop = scTop;
    this.prepareStyle();
  };

  getByPostfixId = postfixId => {
    const { htmlId } = this.state;
    const id = `${ htmlId }${ isNotEmpty(postfixId) ? `-${ postfixId }` : '' }`;
    return document.getElementById(id);
  };

  getRef = name => elm => {
    this[ name ] = elm;
  };

  Content = ({ children, getRef, parentId, propagation = false }) => {
    return (
      <div
        key='content'
        className={ classNames('custom-scrollbars--content') }
        ref={ getRef }
        id={ `${ parentId }-content` }
        onWheel={ this.handleWheel }
      >
        <div className={ classNames('content--scrolled-box') }>
          { children }
        </div>
      </div>
    );
  };

  HorizontalScrollbar = ({ getRef, onClick, parentId }) => {
    return (
      <div
        key='horizontalScrollbar'
        className={ classNames('custom-scrollbars--horizontal-scrollbar') }
        ref={ elm => !!getRef && getRef('hSB')(elm) }
        id={ `${ parentId }-hSB` }
      >
        <div
          key='horizontalBar'
          className={ classNames('horizontal-scrollbar--bar') }
          id={ `${ parentId }-hSBBar` }
          onClick={ onClick(HORIZONTAL) }
        >
          <div
            key='horizontalHandler'
            className={ classNames('horizontal-bar--handler') }
            ref={ elm => !!getRef && getRef('hHnd')(elm) }
            id={ `${ parentId }-hHnd` }
            onClick={ e => { e.stopPropagation(); e.preventDefault(); } }
          />
        </div>
      </div>
    );
  };

  VerticalScrollbar = ({ getRef, onClick, parentId }) => {
    return (
      <div
        key='verticalScrollbar'
        className={ classNames('custom-scrollbars--vertical-scrollbar') }
        ref={ elm => !!getRef && getRef('vSB')(elm) }
        id={ `${ parentId }-vSB` }
      >
        <div
          key='verticalBar'
          className={ classNames('vertical-scrollbar--bar') }
          id={ `${ parentId }-vSBBar` }
          onClick={ onClick(VERTICAL) }
        >
          <div
            key='verticalHandler'
            className={ classNames('vertical-bar--handler') }
            ref={ elm => !!getRef && getRef('vHnd')(elm) }
            id={ `${ parentId }-vHnd` }
            onClick={ e => { e.stopPropagation(); e.preventDefault(); } }
          />
        </div>
      </div>
    );
  };

  render() {
    const {
      className,
      children,
      isHorizontal,
      isVertical,
      disableHorizontalScroll,
      getRef,
      hideScrollbars,
      width,
      height,
      propagation,
    } = this.props;
    const { htmlId } = this.state;
    const Content = this.Content;
    const HorizontalScrollbar = this.HorizontalScrollbar;
    const VerticalScrollbar = this.VerticalScrollbar;
    return (
      <div
        className={ classNames(
          'CustomScrollbars',
          isNotEmpty(className) && className,
          hideScrollbars && 'scrollbars-hide'
        ) }
        style={{ width, height }}
        id={ htmlId }
        ref={ elm => {
          // this.component = elm;
          !!getRef && getRef(elm);
        } }
      >
        <Content
          parentId={ htmlId }
          propagation={ propagation }
        >
          { children }
        </Content>
        {
          isHorizontal && (
            <HorizontalScrollbar
              parentId={ htmlId }
              onClick={
                !disableHorizontalScroll
                  ? this.handleSBClick
                  : null
              }
            />
          )
        }
        {
          isVertical && (
            <VerticalScrollbar
              parentId={ htmlId }
              onClick={ this.handleSBClick }
            />
          )
        }
      </div>
    )
  }
}

export default CustomScrollbars;
