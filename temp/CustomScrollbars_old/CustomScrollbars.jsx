import './CustomScrollbars.scss';
import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isNotEmpty } from '../../utils/utils';

const VERTICAL = 'VERTICAL';
const HORIZONTAL = 'HORIZONTAL';

class CustomScrollbars extends PureComponent {
  static classId = 0;

  static getHtmlId = id => {
    if (id) return id;
    return `CustomScrollbars-${ CustomScrollbars.classId }`;
  };

  static propTypes = {
    className               : PropTypes.string,
    isHorizontal            : PropTypes.bool,
    isVertical              : PropTypes.bool,
    disableHorizontalScroll : PropTypes.bool,
    width                   : PropTypes.oneOfType([
      PropTypes.string, PropTypes.number,
    ]),
    height                  : PropTypes.oneOfType([
      PropTypes.string, PropTypes.number,
    ]),
    hideScrollbars          : PropTypes.bool,
    getRef                  : PropTypes.func,
    getScroller             : PropTypes.func,
    onChange                : PropTypes.func,
  };

  static defaultProps = {
    className               : null,
    isHorizontal            : true,
    isVertical              : true,
    disableHorizontalScroll : false,
    width                   : '100%',
    height                  : '100%',
    hideScrollbars          : false,
    getRef                  : null,
    getScroller             : null,
    onChange                : null,
  };

  constructor(props) {
    super(props);
    CustomScrollbars.classId++;
    this.state = {
      htmlId: CustomScrollbars.getHtmlId(props.id),
    };
  }

  x = 0;
  y = 0;
  mode = null;

  componentDidMount() {
    this.setListeners();
    this.prepareStyle();
    const { getScroller } = this.props;
    !!getScroller && getScroller(this.scroll);
  }

  componentDidUpdate( prevProps, prevState, snapshot ) {
    this.prepareStyle();
  }

  componentWillUnmount() {
    this.removeListeners();
  }

  setListeners = () => {
    const hHnd = this.getByPostfixId('hHnd');
    const vHnd = this.getByPostfixId('vHnd');

    // const hHnd = !!this.hHnd
    //   ? ReactDom.findDOMNode(this.hHnd)
    //   : null;
    // const vHnd = !!this.vHnd
    //   ? ReactDom.findDOMNode(this.vHnd)
    //   : null;
    if (!!hHnd) hHnd.addEventListener('mousedown', this.handleHHndMouseDown, false);
    if (!!vHnd) vHnd.addEventListener('mousedown', this.handleVHndMouseDown, false);
    window.addEventListener('resize', this.handleResize, false);
  };

  removeListeners = () => {
    const hHnd = this.getByPostfixId('hHnd');
    const vHnd = this.getByPostfixId('vHnd');

    // const hHnd = !!this.hHnd
    //   ? ReactDom.findDOMNode(this.hHnd)
    //   : null;
    // const vHnd = !!this.vHnd
    //   ? ReactDom.findDOMNode(this.vHnd)
    //   : null;
    if (!!hHnd) hHnd.removeEventListener('mousedown', this.handleHHndMouseDown);
    if (!!vHnd) vHnd.removeEventListener('mousedown', this.handleVHndMouseDown);
    window.removeEventListener('resize', this.handleResize);
  };

  prepareStyle = () => {
    const content = this.getByPostfixId('content');
    // if (!!this.content) {
    if (!!content) {
      const {
        scrollLeft: cntScLeft,
        scrollTop: cntScTop,
        scrollHeight: cntScHeight,
        scrollWidth: cntScWidth,
        offsetWidth: cntWidth,
        // offsetTop: cntTop,
        offsetHeight: cntHeight,
      // } = this.content;
      } = content;

      // console.log('content: ', content);

      const kHeight = cntHeight / cntScHeight;
      const kWidth = cntWidth / cntScWidth;

      const kLeft = cntScWidth > cntWidth
        ? cntScLeft / (cntScWidth - cntWidth)
        : cntScLeft / cntScWidth;

      const kTop = cntScHeight > cntHeight
        ? cntScTop / (cntScHeight - cntHeight)
        : cntScTop / cntScHeight;

      const hSB = this.getByPostfixId('hSB');
      const hHnd = this.getByPostfixId('hHnd');
      const vSB = this.getByPostfixId('vSB');
      const vHnd = this.getByPostfixId('vHnd');

      // if (!!this.hSB && !!this.hHnd) {
      if (!!hSB && !!hHnd) {
        // let width = this.hSB.offsetWidth * kWidth;
        let width = hSB.offsetWidth * kWidth;
        (width < 32) && (width = 32);
        hHnd.style.width = `${ width }px`;
        hHnd.style.left = `${ (hSB.offsetWidth - width) * kLeft }px`;
        // this.hHnd.style.width = `${ width }px`;
        // this.hHnd.style.left = `${ (this.hSB.offsetWidth - width) * kLeft }px`;
        if (cntWidth >= cntScWidth) {
          // this.hSB.style.visibility = 'hidden';
          // !!this.vSB && (this.vSB.style.height = 'calc(100% - 8px)');
          hSB.style.visibility = 'hidden';
          !!vSB && (vSB.style.height = 'calc(100% - 8px)');
        } else {
          hSB.style.visibility = 'visible';
          !!vSB && (vSB.style.height = 'calc(100% - 20px)');
          // this.hSB.style.visibility = 'visible';
          // !!this.vSB && (this.vSB.style.height = 'calc(100% - 20px)');
        }
      }

      // if (!!this.vSB && !!this.vHnd) {
      if (!!vSB && !!vHnd) {
        // let height = this.vSB.offsetHeight * kHeight;
        let height = vSB.offsetHeight * kHeight;
        (height < 32) && (height = 32);
        vHnd.style.height = `${ height }px`;
        vHnd.style.top = `${ (vSB.offsetHeight - height) * kTop }px`;
        // this.vHnd.style.height = `${ height }px`;
        // this.vHnd.style.top = `${ (this.vSB.offsetHeight - height) * kTop }px`;
        if (cntHeight >= cntScHeight) {
          vSB.style.visibility = 'hidden';
          !!hSB && (hSB.style.width = 'calc(100% - 8px)');
          // this.vSB.style.visibility = 'hidden';
          // !!this.hSB && (this.hSB.style.width = 'calc(100% - 8px)');
        } else {
          // this.vSB.style.visibility = 'visible';
          // !!this.hSB && (this.hSB.style.width = 'calc(100% - 20px)');
          vSB.style.visibility = 'visible';
          !!hSB && (hSB.style.width = 'calc(100% - 20px)');
        }
      }

      const { onChange } = this.props;
      !!onChange && onChange(cntScLeft, cntScTop);
    }
  };

  handleResize = () => {
    this.prepareStyle();
  };

  handleHHndMouseDown = event => {
    event = event || window.event;
    event.stopPropagation();
    event.preventDefault();
    const { clientX } = event;
    this.x = clientX;
    this.mode = HORIZONTAL;
    const hSBBar = this.getByPostfixId('hSBBar');
    const component = this.getByPostfixId();
    !!hSBBar && ( hSBBar.dataset.active = `${ true }`);
    !!component && ( component.dataset.active = `${ true }` );

    // !!this.hSBBar && ( this.hSBBar.dataset.active = `${ true }`);
    // !!this.component && ( this.component.dataset.active = `${ true }` );
    document.addEventListener('mousemove', this.handleDocumentMouseMove, false);
    document.addEventListener('mouseup', this.handleDocumentMouseUp, false);
  };

  handleVHndMouseDown = event => {
    event = event || window.event;
    event.stopPropagation();
    event.preventDefault();
    const { clientY } = event;
    this.y = clientY;
    this.mode = VERTICAL;
    const vSBBar = this.getByPostfixId('vSBBar');
    const component = this.getByPostfixId();
    !!vSBBar && ( vSBBar.dataset.active = `${ true }`);
    !!component && ( component.dataset.active = `${ true }` );

    // !!this.vSBBar && ( this.vSBBar.dataset.active = `${ true }`);
    // !!this.component && ( this.component.dataset.active = `${ true }` );
    document.addEventListener('mousemove', this.handleDocumentMouseMove, false);
    document.addEventListener('mouseup', this.handleDocumentMouseUp, false);
  };

  handleWheel = event => {
    // event.preventDefault();
    event.stopPropagation();
    const { deltaX, deltaY, currentTarget } = event;
    let sign = deltaX / Math.abs( deltaX );
    !this.props.disableHorizontalScroll && (currentTarget.scrollLeft += sign * 100);
    sign = deltaY / Math.abs( deltaY );
    currentTarget.scrollTop += sign * 100;
    this.prepareStyle();
  };

  handleDocumentMouseMove = event => {
    event.stopPropagation();
    event.preventDefault();
    const content = this.getByPostfixId('content');

    // if (!this.content) return null;
    if (!content) return null;
    const { clientX, clientY } = event;
    const {
      offsetWidth,
      offsetHeight,
      scrollWidth,
      scrollHeight,
    } = content; // this.content;

    if (this.mode === HORIZONTAL) {
      const hSB = this.getByPostfixId('hSB');
      const hHnd = this.getByPostfixId('hHnd');

      // if (!this.hSB || !this.hHnd) return null;
      if (!hSB || !hHnd) return null;
      // const { offsetWidth: width } = this.hSB;
      const { offsetWidth: width } = hSB;
      // const sLength = width - this.hHnd.offsetWidth;
      const sLength = width - hHnd.offsetWidth;
      const cLength = scrollWidth - offsetWidth;
      let k = 1;
      if (cLength > 0) k = cLength / sLength;
      const length = clientX - this.x;
      this.x = clientX;
      // this.content.scrollLeft += length * k;
      content.scrollLeft += length * k;
    }
    else if (this.mode === VERTICAL) {
      const vSB = this.getByPostfixId('vSB');
      const vHnd = this.getByPostfixId('vHnd');

      // if (!this.vSB || !this.vHnd) return null;
      if (!vSB || !vHnd) return null;
      const { offsetHeight: height } = vSB; // this.vSB;
      const sLength = height - vHnd.offsetHeight; // this.vHnd.offsetHeight;
      const cLength = scrollHeight - offsetHeight;
      let k = 1;
      if (cLength > 0) {
        k = cLength / sLength;
      }
      const length = clientY - this.y;
      this.y = clientY;
      // this.content.scrollTop += length * k;
      content.scrollTop += length * k;
    }

    this.prepareStyle();
  };

  handleDocumentMouseUp = event => {
    event = event || window.event;
    event.preventDefault();
    event.stopPropagation();
    this.mode = null;
    document.removeEventListener('mousemove', this.handleDocumentMouseMove);
    document.removeEventListener('mouseup', this.handleDocumentMouseUp);
    const { htmlId } = this.state;
    const hSBBar = document.getElementById(`${ htmlId }-hSBBar`);
    const vSBBar = document.getElementById(`${ htmlId }-vSBBar`);
    const component = document.getElementById(htmlId);
    !!hSBBar && ( hSBBar.dataset.active = `${ false }`);
    !!vSBBar && ( vSBBar.dataset.active = `${ false }`);
    !!component && ( component.dataset.active = `${ false }` );

    /*!!this.hSBBar && ( this.hSBBar.dataset.active = `${ false }`);
    !!this.vSBBar && ( this.vSBBar.dataset.active = `${ false }`);
    !!this.component && ( this.component.dataset.active = `${ false }` );*/
  };

  handleSBClick = sb => event => {
    event.stopPropagation();
    event.preventDefault();
    const { clientX, clientY } = event;
    const { htmlId } = this.state;
    const content = document.getElementById(`${ htmlId }-content`);
    if (!content) return null;

    // if (!this.content) return null;
    const {
      offsetWidth,
      offsetHeight,
      scrollWidth,
      scrollHeight,
    } = content; // this.content;

    if (sb === HORIZONTAL) {
      const hSB = document.getElementById(`${ htmlId }-hSB`);
      const hHnd = document.getElementById(`${ htmlId }-hHnd`);
      if (!hSB || !hHnd) return null;

      // if (!this.hSB || !this.hHnd) return null;
      let cLength = 0;
      if (scrollWidth > offsetWidth) cLength = scrollWidth - offsetWidth;
      const { width, left } = hSB.getBoundingClientRect(); // this.hSB.getBoundingClientRect();
      let sLength = 0;
      // if (width > this.hHnd.offsetWidth) sLength = width - this.hHnd.offsetWidth;
      if (width > hHnd.offsetWidth) sLength = width - hHnd.offsetWidth;
      const kx = (cLength > 0) ? sLength / cLength : 1;
      // const x = (clientX - left - this.hHnd.offsetWidth / 2) / kx;
      const x = (clientX - left - hHnd.offsetWidth / 2) / kx;
      // this.content.scrollLeft = x;
      content.scrollLeft = x;
    } else if (sb === VERTICAL) {
      const vSB = document.getElementById(`${ htmlId }-vSB`);
      const vHnd = document.getElementById(`${ htmlId }-vHnd`);

      // if (!this.vSB || !this.vHnd) return null;
      if (!vSB || !vHnd) return null;
      let cLength = 0;
      if (scrollHeight > offsetHeight) cLength = scrollHeight - offsetHeight;
      // const { height, top } = this.vSB.getBoundingClientRect();
      const { height, top } = vSB.getBoundingClientRect();
      let sLength = 0;
      // if (height > this.vHnd.offsetHeight) sLength = height - this.vHnd.offsetHeight;
      if (height > vHnd.offsetHeight) sLength = height - vHnd.offsetHeight;
      const ky = (cLength > 0) ? sLength / cLength : 1;
      // const y = (clientY - top - this.vHnd.offsetHeight / 2) / ky;
      const y = (clientY - top - vHnd.offsetHeight / 2) / ky;
      this.content.scrollTop = y;
    }
    this.prepareStyle();
  };

  scroll = (scLeft, scTop) => {
    const { htmlId } = this.state;
    const content = document.getElementById(`${ htmlId }-content`);
    if (!content) return;
    content.scrollLeft = scLeft;
    content.scrollTop = scTop;

    /*if ( !this.content ) return;
    this.content.scrollLeft = scLeft;
    this.content.scrollTop = scTop;*/
    this.prepareStyle();
  };

  getByPostfixId = postfixId => {
    const { htmlId } = this.state;
    const id = `${ htmlId }${ isNotEmpty(postfixId) ? `-${ postfixId }` : '' }`;
    return document.getElementById(id);
  };

  getRef = name => elm => {
    this[ name ] = elm;
  };

  Content = ({ children, getRef, parentId }) => {
    return (
      <div
        key='content'
        className={ classNames('custom-scrollbars--content') }
        ref={ getRef }
        id={ `${ parentId }-content` }
        onWheel={ this.handleWheel }
      >
        <div className={ classNames('content--scrolled-box') }>
          { children }
        </div>
      </div>
    );
  };

  HorizontalScrollbar = ({ getRef, onClick, parentId }) => {
    return (
      <div
        key='horizontalScrollbar'
        className={ classNames('custom-scrollbars--horizontal-scrollbar') }
        ref={ getRef('hSB') }
        id={ `${ parentId }-hSB` }
      >
        <div
          key='horizontalBar'
          className={ classNames('horizontal-scrollbar--bar') }
          ref={ elm => { this.hSBBar = elm; } }
          id={ `${ parentId }-hSBBar` }
          onClick={ onClick(HORIZONTAL) }
        >
          <div
            key='horizontalHandler'
            className={ classNames('horizontal-bar--handler') }
            ref={ getRef('hHnd') }
            id={ `${ parentId }-hHnd` }
            onClick={ e => { e.stopPropagation(); e.preventDefault(); } }
          />
        </div>
      </div>
    );
  };

  VerticalScrollbar = ({ getRef, onClick, parentId }) => {
    return (
      <div
        key='verticalScrollbar'
        className={ classNames('custom-scrollbars--vertical-scrollbar') }
        ref={ getRef('vSB') }
        id={ `${ parentId }-vSB` }
      >
        <div
          key='verticalBar'
          className={ classNames('vertical-scrollbar--bar') }
          ref={ elm => { this.vSBBar = elm; } }
          id={ `${ parentId }-vSBBar` }
          onClick={ onClick(VERTICAL) }
        >
          <div
            key='verticalHandler'
            className={ classNames('vertical-bar--handler') }
            ref={ getRef('vHnd') }
            id={ `${ parentId }-vHnd` }
            onClick={ e => { e.stopPropagation(); e.preventDefault(); } }
          />
        </div>
      </div>
    );
  };

  render() {
    const {
      className,
      children,
      isHorizontal,
      isVertical,
      disableHorizontalScroll,
      hideScrollbars,
      width,
      height,
    } = this.props;
    const { htmlId } = this.state;
    const Content = this.Content;
    const HorizontalScrollbar = this.HorizontalScrollbar;
    const VerticalScrollbar = this.VerticalScrollbar;
    return (
      <div
        className={ classNames(
          'CustomScrollbars',
          isNotEmpty(className) && className,
          hideScrollbars && 'scrollbars-hide'
        ) }
        style={{ width, height }}
        id={ htmlId }
        ref={ elm => {
          this.component = elm;
          !!this.props.getRef && this.props.getRef(elm);
        } }
      >
        <Content
          parentId={ htmlId }
          getRef={ this.getRef('content') }
        >
          { children }
        </Content>
        {
          isHorizontal && (
            <HorizontalScrollbar
              parentId={ htmlId }
              getRef={ this.getRef }
              onClick={
                !disableHorizontalScroll
                  ? this.handleSBClick
                  : null
              }
            />
          )
        }
        {
          isVertical && (
            <VerticalScrollbar
              parentId={ htmlId }
              getRef={ this.getRef }
              onClick={ this.handleSBClick }
            />
          )
        }
      </div>
    )
  }
}

export default CustomScrollbars;
