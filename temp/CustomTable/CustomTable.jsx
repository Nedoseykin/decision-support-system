import './CustomTable.scss';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import EmptyContent from './EmptyContent/EmptyContent';
import TableContent from './TableContent/TableContent';

import { isNotEmpty, isNotEmptyArray } from '../../utils/utils';

/*
@prop columns = [
 {
   id           : 'id',
   value        : 'ID', // <anyType>
   columnName   : 'Column name', // <String> - for column menu (otherwise)
   visible      : true,
   fixed        : true,
   resizable    : true,
   movable      : true,
   sortable     : false,
   sorted       : 'none',
   sorter       : null,
   tdValueMaker : null,
   tdIdKey      : null,
   width        : 50,
 },
  ...
]

@prop rows = [
  {
    id: 1,
    cells: [
      { id, value },
    ]
  },
  ...
]

@prop emptyContent = any node which should be rendered if columns === [] || null || undefined

@prop cbColumnsChanged = columns => { ... }

@prop cbRowDoubleClicked = rowId => { ... }
*
* */

class CustomTable extends PureComponent {
  static classId = 0;

  static getHtmlId = id => {
    if (id) return id;
    return `CustomTable-${ CustomTable.classId }`;
  };

  static propTypes = {
    className: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    id: PropTypes.oneOfType([
      PropTypes.number, PropTypes.string,
    ]),
    rows: PropTypes.arrayOf(PropTypes.object).isRequired,
    emptyContent: PropTypes.any,
    cbColumnsChanged: PropTypes.func,
    cbRowDoubleClicked: PropTypes.func,
  };

  static defaultProps = {
    className: null,
    id: null,
    emptyContent: null,
    cbColumnsChanged: null,
    cbRowDoubleClicked: null,
  };

  constructor(props) {
    super(props);
    CustomTable.classId++;
    this.state = {
      htmlId: CustomTable.getHtmlId(props.id),
    };
    // this.htmlId = `CustomTable-${ CustomTable.classId }`;
  }

  render() {
    const {
      className,
      columns,
      rows,
      emptyContent,
      cbColumnsChanged,
      cbRowDoubleClicked,
    } = this.props;
    const { htmlId } = this.state;

    return (
      <div
        className={ classNames('CustomTable', isNotEmpty(className) && className ) }
        id={ htmlId }
      >
        {
          isNotEmptyArray( columns )
            ? <TableContent
                columns={ columns }
                rows={ rows }
                htmlId={ htmlId }
                parentId={ htmlId }
                cbColumnsChanged={ cbColumnsChanged }
                cbRowDoubleClicked={ cbRowDoubleClicked }
              />
            : (
                <EmptyContent>
                  { emptyContent }
                </EmptyContent>
              )
        }
      </div>
    )
  }
}

export default CustomTable;
