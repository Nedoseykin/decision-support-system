import React from 'react';

const EmptyContent = ({ children }) => {
  return (
    <div className='custom-table--empty-content'>
      { children }
    </div>
  )
};

export default EmptyContent;
