import './ShadowTh.scss'
import React from 'react';

const ShadowTh = props => {
  return (
    <div className='ShadowTh' >
      { props.children }
    </div>
  )
};

export default ShadowTh;
