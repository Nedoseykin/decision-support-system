import React, { Fragment, useState } from 'react';

import Scrollbars from '../../CustomScrollbars/CustomScrollbars';
import Th from './Th/Th';
import ShadowTh from './ShadowTh/ShadowTh';
import ColumnsMenu from './ColumnsMenu/ColumnsMenu';

import { findArrayItem, findArrayItemIndex, isNotEmptyArray } from '../../../utils/utils';

const TableContent = ({ columns, rows, htmlId, parentId, cbColumnsChanged, cbRowDoubleClicked }) => {
  const [ contextMenuData, setContextMenuData ] = useState({ open: 'initial', x: 0, y: 0 });
  
  let scrollableHead = null;
  let fixedContent = null;

  let scrollableScroller = null;

  const calculateTableData = columns => {
    const fixed = columns.filter(c => ( c.fixed && c.visible ));
    let fixedWidth = 0;
    fixed.forEach(f => { fixedWidth += f.width });
    const scrollable = columns.filter(c => (!c.fixed && c.visible));
    let scrollableFullWidth = 0;
    scrollable.forEach(s => { scrollableFullWidth += s.width });
    return { fixed, fixedWidth, scrollable, scrollableFullWidth };
  };

  const {
    fixed,
    scrollable,
    fixedWidth,
    scrollableFullWidth,
  } = calculateTableData(columns);

  const cbColumnWidthChanged = (columnId, columnWidth) => {
    let newColumns = columns;
    if (isNotEmptyArray(columns)) {
      newColumns = columns.map(c => ({
        ...c,
        width: c.id === columnId ? columnWidth : c.width,
      }))
    }
    !!cbColumnsChanged && cbColumnsChanged(newColumns);
  };

  const handleRowDoubleClick = rowId => () => {
    !!cbRowDoubleClicked && cbRowDoubleClicked( rowId );
  };

  const handleColumnOrderChange = ordered => {
    let newColumns = [];
    columns = columns.slice();
    ordered.forEach(item => {
      let { id, fixed } = item;
      fixed = fixed === 'true';
      const i = findArrayItemIndex(columns, { id });
      if (i > -1) {
        const c = columns.splice(i, 1)[ 0 ];
        newColumns.push({ ...c, fixed });
      }
    });
    newColumns = newColumns.concat(columns);
    !!cbColumnsChanged && cbColumnsChanged(newColumns);
  };

  const handleToggleContextMenu = open => event => {
    event.preventDefault();
    event.stopPropagation();
    let { x, y } = contextMenuData;
    if (open === true) {
      const { clientX, clientY } = event;
      const table = document.getElementById(htmlId);
      if (!table)return;
      const menu = table.querySelector('.ColumnsMenu');
      if (!menu) return;
      const { left: tLeft, top: tTop } = table.getBoundingClientRect();
      x = clientX - tLeft;
      y = clientY - tTop;
    }
    setContextMenuData({ open, x, y });
  };

  const cbScrollableChangeScroll = (scLeft, scTop) => {
    if (!!fixedContent) {
      const elm = fixedContent.querySelector('.custom-scrollbars--content');
      if ( !!elm ) {
        elm.scrollTop = scTop;
      }
    }
    if (!!scrollableHead) {
      scrollableHead.scrollLeft = scLeft;
    }
  };

  const cbFixedChangeScroll = (scLeft, scTop) => {
    if ( !!scrollableScroller ) scrollableScroller(scLeft, scTop);
  };

  const FixedTable = props => {
    const {
      columns,
      rows,
      width,
      htmlId,
      parentId,
      cbColumnWidthChanged,
      onRowDoubleClick,
      cbColumnOrderChanged,
      cbColumnContextMenuClicked,
    } = props;
    return isNotEmptyArray( columns )
      ? (
        <div className='custom-table--fixed' style={{ width }}>
          <div
            key='fixedHeader'
            className='fixed--head'
          >
            <div
              key='head-row'
              className='custom-table--tr'
            >
              {
                columns.map(th => (
                  <Th
                    key={ th.id }
                    htmlId={ htmlId }
                    { ...th }
                    cbColumnWidthChanged={ cbColumnWidthChanged }
                    cbColumnOrderChanged={ cbColumnOrderChanged }
                    cbColumnContextMenuClicked={ cbColumnContextMenuClicked }
                  />
                ))
              }
            </div>
          </div>
          <div
            key='fixedBody'
            className='fixed--body'
          >
            <Scrollbars
              isHorizontal={ false }
              isVertical={ false }
              disableHorizontalScroll={ true }
              getRef={ elm => { fixedContent = elm } }
              onChange={ cbFixedChangeScroll }
            >
              {
                isNotEmptyArray(rows) && (
                  rows.map(tr => {
                    const { id: trId, cells } = tr;
                    return (
                      <div
                        key={ `tr-${ trId }` }
                        className='custom-table--tr'
                        onDoubleClick={ onRowDoubleClick( trId ) }
                      >
                        {
                          columns.map(td => {
                            const { id, width } = td;
                            const cell = findArrayItem( cells, { id } );
                            const columnHtmlId = `${ htmlId }-${ id }`;
                            const value = !!cell ? cell.value : null;
                            return (
                              <div
                                key={ `td-${ id }` }
                                className='custom-table--td'
                                data-column_id={ columnHtmlId }
                                style={{ width: width }}
                              >
                                { value }
                              </div>
                            )
                          })
                        }
                      </div>
                    )
                  })
                )
              }
            </Scrollbars>
          </div>
        </div>
      )
      : null;
  };

  const ScrollableTable = props => {
    const {
      columns,
      rows,
      fullWidth,
      fixedWidth,
      htmlId,
      parentId,
      cbColumnWidthChanged,
      onRowDoubleClick,
      cbColumnOrderChanged,
      cbColumnContextMenuClicked,
    } = props;
    return (
      <div className='custom-table--scrollable' style={{ width: `calc(100% - ${ fixedWidth }px)` }}>
        <div
          key='scrollable-head'
          className='scrollable--head'
          style={{ width: '100%' }}
          ref={ elm => { scrollableHead = elm } }
        >
          <div
            key={ `head-row` }
            className='custom-table--tr'
            style={{ width: fullWidth }}
          >
            {
              columns.map(th => (
                <Th
                  key={ th.id }
                  htmlId={ htmlId }
                  { ...th }
                  cbColumnWidthChanged={ cbColumnWidthChanged }
                  cbColumnOrderChanged={ cbColumnOrderChanged }
                  cbColumnContextMenuClicked={ cbColumnContextMenuClicked }
                />
              ))
            }
          </div>
        </div>
        <div
          key='scrollable-body'
          className='scrollable--body'
          style={{ width: '100%' }}
        >
          <Scrollbars
            hideScrollbars={ true }
            getRef={ elm => { /*scrollableContent = elm*/ } }
            getScroller={ scroller => { scrollableScroller = scroller; } }
            onChange={ cbScrollableChangeScroll }
          >
            {
              isNotEmptyArray(rows) &&
              rows.map(tr => {
                const { id: trId, cells } = tr;
                return (
                  <div
                    key={ `body-row-${ trId }` }
                    className='custom-table--tr'
                    style={{ width: fullWidth }}
                    onDoubleClick={ onRowDoubleClick( trId ) }
                  >
                    {
                      columns.map(td => {
                        const { id, width } = td;
                        const cell = findArrayItem( cells, { id } );
                        const columnHtmlId = `${ htmlId }-${ id }`;
                        const value = !!cell ? cell.value : null;
                        return (
                          <div
                            key={ id }
                            className='custom-table--td'
                            data-column_id={ columnHtmlId }
                            style={{ width }}
                          >
                            { value }
                          </div>
                        )
                      })
                    }
                  </div>
                )
              })
            }
          </Scrollbars>
        </div>
      </div>
    );
  };


  return (
    <Fragment>
      <FixedTable
        parentId={ parentId }
        htmlId={ htmlId }
        columns={ fixed }
        rows={ rows }
        width={ fixedWidth }
        cbColumnWidthChanged={ cbColumnWidthChanged }
        onRowDoubleClick={ handleRowDoubleClick }
        cbColumnOrderChanged={ handleColumnOrderChange }
        cbColumnContextMenuClicked={ handleToggleContextMenu(true) }
      />
      <ScrollableTable
        parentId={ parentId }
        htmlId={ htmlId }
        columns={ scrollable }
        rows={ rows }
        fullWidth={ scrollableFullWidth }
        fixedWidth={ fixedWidth }
        cbColumnWidthChanged={ cbColumnWidthChanged }
        onRowDoubleClick={ handleRowDoubleClick }
        cbColumnOrderChanged={ handleColumnOrderChange }
        cbColumnContextMenuClicked={ handleToggleContextMenu(true) }
      />
      <ShadowTh />
      <ColumnsMenu
        open={ contextMenuData.open }
        x={ contextMenuData.x }
        y={ contextMenuData.y }
        columns={ columns }
        cbChanged={ cbColumnsChanged }
        cbClosed={ handleToggleContextMenu(false) }
      />
    </Fragment>
  )
};

export default TableContent;
