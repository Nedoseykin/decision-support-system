import React from 'react';
import classNames from 'classnames';
import { findArrayItemIndex, isNotEmpty } from '../../../../utils/utils';

const Th = props => {
  const {
    htmlId,
    id,
    value,
    resizable,
    movable,
    fixed,
    // sortable,
    // sorted,
    // sorter,
    width,
    cbColumnWidthChanged, // callback after columns width has been changed
    cbColumnOrderChanged, //callback after columns order has been changed
    cbColumnContextMenuClicked,
  } = props;

  let table  = null;
  let startX = 0;
  let startY = 0;
  let posX   = 0;
  let posY   = 0;
  let startWidth = 0;
  let refTh = null;
  let refShadowTh = null;

  const columnHtmlId = `${ htmlId }-${ id }`;

  const resizeFixed = (newWidth, newTrWidth, delta, partTable, table, cols) => {
    const scrollableTable = table.querySelector('.custom-table--scrollable');
    partTable.style.width = `${ newTrWidth }px`;
    !!scrollableTable && (
      scrollableTable.style.width = `calc(100% - ${ newTrWidth }px)`
    );
    if (!isNotEmpty(cols)) return;
    Array.prototype.forEach.call(cols, td => {
      // const tr = td.parentNode;
      td.style.width = `${ newWidth }px`;
      // tr.style.width = `${ newTrWidth }px`;
    });
  };

  const resizeScrollable = (newWidth, newTrWidth, delta, partTable, table, cols) => {
    let tr = refTh.parentNode;
    if (!tr) return;
    tr.style.width = `${ newTrWidth }px`;
    if (!isNotEmpty(cols)) return;
    Array.prototype.forEach.call(cols, td => {
      if (!td) return;
      tr = td.parentNode;
      !!tr && (tr.style.width = `${ newTrWidth }px`);
      td.style.width = `${ newWidth }px`;
    });
  };

  const resizeTable = (newWidth) => {
    // console.log('refTh: ', refTh);
    if (!refTh) return;
    const oldColumnWidth = refTh.offsetWidth;
    const delta = newWidth - oldColumnWidth;
    if ( delta === 0 ) return;

    const { column_id } = refTh.dataset;
    // console.log('column_id: ', column_id);

    const headTr = refTh.parentNode;
    if (!headTr) return;
    const trTds = headTr.querySelectorAll('.custom-table--th');
    let newTrWidth = 0;
    Array.prototype.forEach.call(trTds, td => { newTrWidth += td.offsetWidth; });
    newTrWidth += delta;

    const head = headTr.parentNode;
    if (!head) return;
    const partTable = head.parentNode;
    if (!partTable) return;

    const table = document.querySelector(`#${ htmlId }`);
    // console.log('table: ', table);
    if (!table) return;
    const cols = table.querySelectorAll(`.custom-table--td[data-column_id="${ column_id }"]`);

    refTh.style.width = `${ newWidth }px`;

    if (fixed) {
      resizeFixed(newWidth, newTrWidth, delta, partTable, table, cols);
    } else {
      resizeScrollable(newWidth, newTrWidth, delta, partTable, table, cols);
    }
  };

  const handleResizeMouseMove = event => {
    event.preventDefault();
    event.stopPropagation();
    if ( !refTh ) return;
    const { clientX } = event;
    const delta = clientX - startX;
    let newWidth = startWidth + delta;
    if ( newWidth < 32 ) newWidth = 32;
    resizeTable(newWidth);
  };

  const handleResizeMouseUp = event => {
    event.preventDefault();
    event.stopPropagation();
    document.removeEventListener('mousemove', handleResizeMouseMove);
    document.removeEventListener('mouseup', handleResizeMouseUp);
    if (!!refTh) {
      const { offsetWidth } = refTh;
      refTh = null;
      !!cbColumnWidthChanged && cbColumnWidthChanged(id, offsetWidth);
    }
  };

  const handleResizeMouseDown = event => {
    event.preventDefault();
    event.stopPropagation();
    document.addEventListener('mousemove', handleResizeMouseMove, false);
    document.addEventListener('mouseup', handleResizeMouseUp, false);
    const { clientX, currentTarget } = event;
    refTh = currentTarget.parentNode;
    if (!refTh) return;
    startWidth = refTh.offsetWidth;
    startX = clientX;
  };

  const handleMoveMouseMove = event => {
    event.preventDefault();
    event.stopPropagation();
    const { clientX, clientY } = event;
    if (!refShadowTh || !table) return;
    const { top, left } = table.getBoundingClientRect();
    refShadowTh.style.left = `${ clientX - left - posX }px`;
    refShadowTh.style.top = `${ clientY - top - posY }px`;
  };

  const handleMoveMouseUp = event => {
    event.preventDefault();
    event.stopPropagation();
    document.removeEventListener('mousemove', handleMoveMouseMove);
    document.removeEventListener('mouseup', handleMoveMouseUp);
    if (!!refShadowTh && !!refTh && !!table) {
      const { clientX: x } = event;
      const thId = refTh.dataset.id;
      const { left: thLeft } = refTh.getBoundingClientRect();
      const { left: shLeft } = refShadowTh.getBoundingClientRect();
      const deltaX = shLeft - thLeft;
      // -1 === to left, 0 === stay, 1 === to right
      const direction = deltaX !== 0 ? deltaX / Math.abs(deltaX) : 0;

      let fixed = table.querySelectorAll('.fixed--head .custom-table--th');
      let scrollable = table.querySelectorAll('.scrollable--head .custom-table--th');
      let ths = Array.prototype.concat.apply([], fixed);
      ths = ths.concat.apply(ths, scrollable)
        .map(th => ({
          id: th.dataset.id,
          fixed: th.dataset.fixed,
          rect: th.getBoundingClientRect(),
        }));

      if (direction !== 0) {
        const i = findArrayItemIndex(ths, { id: thId });
        // const i = ths.findIndex(th => th.id === thId);
        let newI = i;
        if ((direction === 1) && (i < ths.length - 1)) {
          for (let j = i + 1; j < ths.length; j++) {
            const { left, width } = ths[ j ].rect;
            if ((x > left) && x < (left + width)) {
              newI = j;
              break;
            }
          }
          if (
            (newI === i)
            && (x > ths[ ths.length - 1 ].rect.left + ths[ ths.length - 1 ].rect.width)
          ) { newI = ths.length - 1 }
          if (newI !== i) {
            const fixedValue = ths[ newI ].fixed;
            ths.splice(newI + 1, 0, { id: thId, fixed: fixedValue });
            ths.splice(i, 1);
          }
        } else if (i > 0) {
          for (let j = i - 1; j >= 0; j--) {
            const { left, width } = ths[ j ].rect;
            if ((x < left + width) && (x > left)) {
              newI = j;
              break;
            }
          }
          if ((newI === i) && (x < ths[ 0 ].rect.left)) { newI = 0 }
          if (newI !== i) {
            const fixedValue = ths[ newI ].fixed;
            ths.splice(newI, 0, { id: thId, fixed: fixedValue });
            ths.splice(i + 1, 1);
          }
        }
      }

      let columnIds = ths.map(th => ({ id: th.id, fixed: th.fixed }));
      !!cbColumnOrderChanged && cbColumnOrderChanged(columnIds);

      refShadowTh.style.width = 0;
      refShadowTh.style.height = 0;
      refShadowTh.dataset.moving = `${ false }`;
    }
  };

  const handleMoveMouseDown = event => {
    event.preventDefault();
    event.stopPropagation();
    document.addEventListener('mousemove', handleMoveMouseMove, false);
    document.addEventListener('mouseup', handleMoveMouseUp, false);
    const { currentTarget, clientX, clientY } = event;
    startX = clientX;
    startY = clientY;
    refTh = currentTarget.parentNode;
    const { width, height, top, left } = refTh.getBoundingClientRect();
    posX = startX - left;
    posY = startY - top;
    table = document.getElementById(htmlId);
    const { top: tableTop, left: tableLeft } = table.getBoundingClientRect();
    refShadowTh = !!table ? table.querySelector('.ShadowTh') : null;
    refShadowTh.style.width = `${ width }px`;
    refShadowTh.style.height = `${ height }px`;
    refShadowTh.style.top = `${ top - tableTop }px`;
    refShadowTh.style.left = `${ left - tableLeft }px`;
    refShadowTh.dataset.moving = `${ true }`;
  };

  // const handleContextMenu = event => {
  //   event.preventDefault();
  //   event.stopPropagation();
  //   const { clientX, clientY } = event;
  //   !!cbColumnContextMenuClicked && cbColumnContextMenuClicked(id, clientX, clientY);
  // };

  return (
    <div
      data-id={ `${ id }` }
      data-column_id={ columnHtmlId }
      data-fixed={ `${ fixed }` }
      className={ classNames('custom-table--th', movable && 'th--movable') }
      style={{ width }}
      onContextMenu={ !!cbColumnContextMenuClicked ? cbColumnContextMenuClicked : null }
    >
      { value }
      {
        movable && (
          <div
            key='movingHandler'
            className='th--move-handler'
            onMouseDown={ handleMoveMouseDown }
          >
          </div>
        )
      }
      {
        resizable && (
          <div
            key='resizeHandler'
            className='th--resize-handler'
            onMouseDown={ handleResizeMouseDown }
          />
        )
      }
    </div>
  )
};

export default Th;
