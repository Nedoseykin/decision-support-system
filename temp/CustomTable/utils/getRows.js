import { isNotEmptyArray } from '../../../utils/utils';

export const getRows = (rowsData, columnsData) => {
  let rows = isNotEmptyArray( rowsData ) && isNotEmptyArray( columnsData )
    ? rowsData.map(row => {
      const { id: rowId } = row;
      return {
        id: rowId,
        cells: columnsData.map(col => {
          const {
            id: colId,
            tdValueMaker,
            tdIdKey,
          } = col;
          const idKey = isNotEmpty( tdIdKey ) ? tdIdKey : colId;
          const value = !!tdValueMaker ? tdValueMaker(row[ idKey ]) : row[ idKey ];
          return {
            id: colId,
            value,
          }
        })
      }
    })
    : [];

  return rows;
};
